# Computer Vision Utilities

This repository provides a collection of tools and utilities aimed at simplifying and enhancing the development of computer vision applications. 

## Project Description

The project contains several modules designed to streamline various aspects of computer vision projects, ranging from data preprocessing, model evaluation to automated annotation and verification.

### Auto Annotation and Verification

- [ ] The [auto_annotation_and_verification](https://gitlab.com/muhammetp/computer_vision_utilities/-/tree/main/auto_annotation_and_verification) module provides functionalities to automate the annotation process of images and verify their accuracy. This significantly reduces manual intervention and enhances the speed and efficiency of labeling tasks.

### Classifiers

- [ ] The [classifiers](https://gitlab.com/muhammetp/computer_vision_utilities/-/tree/main/classifiers) module consists of various pre-configured machine learning models that can be readily utilized for different computer vision tasks. It simplifies the process of defining and training your classifiers.

### Data Cleanup

- [ ] The [data_cleanup](https://gitlab.com/muhammetp/computer_vision_utilities/-/tree/main/data_cleanup) module helps in preprocessing the image datasets. It includes functions to handle common tasks such as image resizing, normalization, augmentation, and handling missing data.

### Design Classes

- [ ] The [design_classes](https://gitlab.com/muhammetp/computer_vision_utilities/-/tree/main/design_classes) module consists of a collection of custom classes and functions designed to aid in the design and implementation of object detection and classification tasks.

### Model Evaluator
- [ ] The [model_evaluator](https://gitlab.com/muhammetp/computer_vision_utilities/-/tree/main/model_evaluator) module consists 
provides a suite of tools to evaluate the performance of your trained models. It provides functionalities to calculate various metrics like precision, recall, F1-score, and others. It also includes functions for generating confusion matrices and ROC curves.

## Installation

To get started with these utilities, follow the steps below:

```bash
# Clone the repository
git clone https://gitlab.com/muhammetp/computer_vision_utilities.git

# Navigate to the project directory
cd computer_vision_utilities

# Build the Docker image
docker build -t cv_utilities .
```

## Usage
```
Here is an example of how to use these utilities:
# Import the necessary modules
from cv_utilities import auto_annotation_and_verification
from cv_utilities import classifiers
from cv_utilities import data_cleanup
from cv_utilities import design_classes
from cv_utilities import model_evaluator

# Use the module's functionalities

```
## Contribution
Contributions to this project are welcome. Please open an issue to discuss your proposed changes before submitting a pull request.

## License

MIT License

Copyright (c) [2022] [Muhammet Pakyurek]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

