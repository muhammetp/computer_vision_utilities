import os
import glob
import string
import sys

import xmltodict
import pickle

from tqdm import tqdm
from pathlib import Path
from logging import warning

"""
As you can see, each row consists of five elements:
Filename, Starting x-coordinate, Starting y-coordinate, Ending x-coordinate ,Ending y-coordinate
"""
not_class = set()
annots = []
classes = ["person_standing", "spill", "handgun", "person_sitting", "snow", "person_fallen", "vehicle"]
#classes = ["weapon", "handgun", "gun", "long_gun"]

def parse_data(path, class_name, bbox):
    global annots
    annots.append([path, bbox["xmin"], bbox["ymin"], bbox["xmax"], bbox["ymax"], class_name])


def accumulate_annotations(main_dir):
    anno_dir_path = main_dir / "xml"  # os.path.join(loc, "xml")
    if not os.path.isdir(anno_dir_path):
        return None
    print(anno_dir_path)
    annotation_paths = os.listdir(anno_dir_path)
    img_name_dict = get_image_dict(main_dir)
    if img_name_dict is None:
        return None
    for anno_path in annotation_paths:
        anno_path = anno_dir_path / anno_path  # os.path.join(anno_dir_path, anno_path)
        with open(anno_path, "r") as fd:
            annotation = xmltodict.parse(fd.read())
            filename = anno_path.stem
            if "object" in annotation['annotation'].keys():
                objs = annotation['annotation']['object']

                if filename in img_name_dict:
                    path = main_dir / "images" / img_name_dict[filename]
                else:
                    warning(f"{filename} doesnt exist in {main_dir}/images")
                    continue
                # path = path.replace("xml", "images")
                if not isinstance(objs, list):
                    objs = [objs]
                for obj in objs:
                    name = obj["name"].lower()
                    if name not in classes:
                        not_class.add(name)
                        continue
                    bbox = obj["bndbox"]
                    parse_data(path, name, bbox)
            else:
                warning(f"{filename} is not suitable xml annotation format!!!")
    return True


def get_image_dict(folder_path: Path):
    img_dir_path = folder_path / "images"
    if not os.path.isdir(img_dir_path):
        return None
    img_names = os.listdir(img_dir_path)
    return {Path(img_nm).stem: img_nm for img_nm in img_names}


def create_annotations_pickle(_locations_folder: Path, annot_filename: string = "annotations.pickle"):
    locations_folder_str = _locations_folder.__str__()
    """ walk through the whole folders """
    for loc in tqdm(glob.glob(locations_folder_str + '/*/'), file=sys.stdout,
                    desc='Adding annotations and renaming locations'):
        folder_main_dir = Path(loc)

        if accumulate_annotations(folder_main_dir) is None:
            warning(f"{folder_main_dir}/images not exist")
            continue

    with open(_locations_folder / annot_filename, 'wb') as handle:
        pickle.dump(annots, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return annots


if __name__ == "__main__":
    locations_folder = Path("C:/projects/data/full_size_master")
    create_annotations_pickle(locations_folder)
    print(not_class)
