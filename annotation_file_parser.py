import json
from pathlib import Path
import cv2
import os
from pascal_voc_writer import Writer
from detector_inference_util import write_table_into_file
from dataclasses import dataclass
from video_creator import get_all_file_paths
from typing import Union
import xmltodict

cls2centernet_dict = {'person_fallen': 'person_on_ground', 'person_sitting': 'person1', 'person_standing': 'person',
                      'snow': 'slip-risk', 'spill': 'slip_risk', 'vehicle': 'vehicle', 'weapon': 'weapon',
                      'unknown': 'unknown', 'handgun': 'weapon', 'backpack': 'backpack'}

def calculate_size_from_json(json_dir)->tuple:
    json_fname = os.listdir(json_dir)[0]
    json_fpath = json_dir / json_fname
    detections, video_size = parse_json_file(json_fpath)
    return video_size

@dataclass
class XMLFileGenerator:
    xml_dir_path: Path
    img_size: tuple

    def __init__(self, json_dir_path: Path, im_size: tuple = None):
        self.img_size = calculate_size_from_json(json_dir_path) if im_size is None else im_size
        self.xml_dir_path = json_dir_path.parent.resolve() / "xml_out"
        self.xml_dir_path.mkdir(parents=True, exist_ok=True)

    def create_xml_file(self, img_path: Path, detections: tuple):
        if len(detections) < 1:
            return
        w, h = self.img_size
        img_name = img_path.name
        xml_path = self.xml_dir_path / (img_name + ".xml")
        writer = Writer(img_path, w, h)
        for detection in detections:
            pred, bbox = parse_box_and_label(detection)
            bbox = bbox[0] + bbox[1]
            writer.addObject(pred, *bbox)

        if writer is not None:
            writer.save(xml_path)


def parse_json_file(filepath: Path) -> Union[list, tuple]:
    with open(filepath) as f:
        all_json_objs = json.load(f)
        detections = all_json_objs["objects"]
        video_size = all_json_objs["size"]; video_size = video_size["width"], video_size["height"]
    return filter_detections(detections), video_size


def filter_detections(detections: tuple):
    keywords = ["person", "weapon"];
    filt_detect = []
    for detection in detections:
        label, bbox = parse_box_and_label(detection)
        #if (keywords[0] in label) or keywords[1] == label:
        filt_detect.append(detection)
    return filt_detect


def parse_box_and_label(detection: dict):
    label = detection["classTitle"]
    bbox = detection["points"]["exterior"]
    return label, bbox


def display_detections(img_fpath, detections: tuple):

    impath = img_fpath.__str__() if isinstance(img_fpath, Path) else img_fpath.copy()
    img = cv2.imread(impath)
    for i, detection in enumerate(detections):
        label, bbox = parse_box_and_label(detection)
        if label is None:
            continue
        res = cv2.rectangle(img, bbox[0], bbox[1], (0, 0, 255))
        res = cv2.putText(res, label, bbox[0], cv2.FONT_HERSHEY_SIMPLEX, 1,
                          (255, 0, 0), 2, cv2.LINE_AA)
    cv2.imshow(f"detection", res)
    cv2.waitKey(200)

def insert_detection_into_table(detections:tuple, table:list, index: Union[int, str], model_name:str = "ground_truth"):
    for detection in detections:
        class_name, bbox = parse_box_and_label(detection)
        bbox = bbox[0] + bbox[1]
        table.append((index, model_name, class_name, -1, *bbox))

def main_quantigo_file_parsing(json_dir):
    out_dir = json_dir.parent
    out_file_name = out_dir / (out_dir.name + "_gt.parquet")
    img_dir = out_dir / "img"
    json_fnames = get_all_file_paths(json_dir)
    xml_handler = XMLFileGenerator(json_dir)
    detection_table = []
    for jfname in json_fnames:
        json_fpath = json_dir / jfname
        img_fpath = img_dir / json_fpath.stem
        detections, video_size = parse_json_file(json_fpath)
        xml_handler.create_xml_file(img_fpath, detections)
        insert_detection_into_table(detections, detection_table, img_fpath.name)
        # display_detections(img_fpath, detections)
    write_table_into_file(detection_table, out_file_name)

def main_xml_annotation(xml_dir):
    xml_fnames = get_all_file_paths(xml_dir)
    xml_handler = XMLFileHandler(xml_dir)
    detection_table = []
    for xml_name in xml_fnames:
        detect_objs = xml_handler.read_and_extract_objects_from_xml(xml_name)
        xml_handler.insert_detections_into_table(detect_objs, detection_table)
    xml_handler.write_table(detection_table)

@dataclass
class XMLFileHandler:
    _out_dir_path: Path
    _xml_dir_path: Path
    _img_size: tuple
    _file_name: str

    def __init__(self, xml_dir_path: Path):
        self._xml_dir_path = xml_dir_path
        self._out_dir_path = xml_dir_path.parent.resolve() / "xml_out"
        self._out_dir_path.mkdir(parents=True, exist_ok=True)

    def read_and_extract_objects_from_xml(self, xml_fname):
        xml_path = self._xml_dir_path / xml_fname
        with open(xml_path) as file:
            file_data = file.read()  # read file contents
            # parse data using package
            dict_data = xmltodict.parse(file_data)['annotation']
            self._file_name = dict_data['filename']
            self._img_size = int(dict_data["size"]["width"]), int(dict_data["size"]["height"])
            return dict_data['object']

    def _acquired_detections_from_objects(self, detect_objs: list):
        bboxes = []; scores = []; preds = []
        if not isinstance(detect_objs, list):
            detect_objs = [detect_objs]
        for obj in detect_objs:
            bbox = [int(float(cord) + 0.5) for cord in obj['bndbox'].values()]
            class_name = cls2centernet_dict[obj['name']]
            bboxes.append(bbox)
            preds.append(class_name)
            scores.append(-1.0)
        return bboxes, scores, preds

    def insert_detections_into_table(self, detect_objs, table):
        bboxes, scores, class_names = self._acquired_detections_from_objects(detect_objs)
        for bbox, score, class_name in zip(bboxes, scores, class_names):
            table.append((self._file_name, "ground_truth", class_name, -1, *bbox))
    def write_table(self, detection_table):
        out_file_name = self._out_dir_path / (self._out_dir_path.parent.name + "_gt.parquet")
        write_table_into_file(detection_table, out_file_name)

if __name__ == "__main__":
    #xml_dir = Path("C:/Users/MalumAdmin/Downloads/test_dataset/xml")
    json_dir = Path("C:/projects/data/quantigo_broadlown_data_dynamic_ar/broadlawns_vertical2_1280x720_padded/ann")
    main_quantigo_file_parsing(json_dir)
