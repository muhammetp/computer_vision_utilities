from rect_intersect_handler import RectIntersectHandler
class NonMaximaSupression:
    intersect_handler: RectIntersectHandler
    label_ind: int
    merging_thresh: float

    def __init__(self, label_ind=2, merging_thresh=0.7):
        self.intersect_handler = RectIntersectHandler()
        self.label_ind = label_ind
        self.merging_thresh = merging_thresh
    def grouping_inferences(self, model_rows: list):
        grouped_inf = {}
        label_ind = self.label_ind
        for row in model_rows:
            label = row[label_ind]
            if label not in grouped_inf:
                grouped_inf[label] = [row]
            else:
                grouped_inf[label].append(row)
        return grouped_inf

    def find_the_same_objects(self, check_obj, search_space: list, check_list: list):
        if check_obj not in check_list: return False
        check_bbox = check_obj[4:]
        merged_objects = [check_obj]
        for search_obj in search_space:
            if search_obj not in check_list: continue
            search_bbox = search_obj[4:]
            iou_val = self.intersect_handler.calculate_iou(check_bbox, search_bbox)
            if iou_val > self.merging_thresh:
                merged_objects.append(search_obj)
        return merged_objects
    def merge_objects(self, to_be_merged_objs, all_objs:list):
        if len(to_be_merged_objs) < 2:
            return None
        highest_prob = to_be_merged_objs[0][-5]
        for i in range(1, len(to_be_merged_objs)):
            next_obj_prob = to_be_merged_objs[i][-5]
            if next_obj_prob > highest_prob:
                all_objs.remove(to_be_merged_objs[i-1])
                highest_prob = next_obj_prob
        return all_objs
    def non_maxima_suppression(self, same_targ_objs: list):
        for ind, check_obj in enumerate(same_targ_objs[:]):
            search_space = same_targ_objs[ind + 1:]
            to_be_merged_objs = self.find_the_same_objects(check_obj, search_space, same_targ_objs)
            same_targ_objs = self.merge_objects(to_be_merged_objs, same_targ_objs)
        return same_targ_objs

    def remove_redundancy_on_inference(self, model_rows: list):
        grouped_rows = self.grouping_inferences(model_rows)
        for key in grouped_rows:
            reduced_rows = self.non_maxima_suppression(grouped_rows[key])
            grouped_rows[key] = reduced_rows
