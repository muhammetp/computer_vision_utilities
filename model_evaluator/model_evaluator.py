import os
import shutil

from pathlib import Path
from typing import Union, List
import pandas as pd
from read_handler import ReadHandler
from fscore_calculator import FScoreCalculator
from detector_inference_util import class_labels_name
pd.set_option('max_columns', None)
# training2deployment_dict = {'person_fallen': 'person_on_ground', 'person_sitting': 'person1', 'person_standing': 'person',
#                       'snow': 'slip-risk', 'spill': 'slip_risk', 'vehicle': 'vehicle', 'weapon': 'weapon',
#                       'handgun': 'handgun', 'person_w/weapon': 'person', 'weapons_guns': 'weapon', "longgun":"longgun"}



def get_same_group_annots(main_dir, kw):
    file_reader = ReadHandler()
    files = [x for x in os.listdir(main_dir) if kw in x]
    dfs = [file_reader.parquet_read(main_dir / (file_name)) for file_name in files]
    return pd.concat(dfs).sort_values(by=['frame', 'model'])


def get_inferred_and_gt_annots(main_dir) -> List[pd.DataFrame]:
    kwords = ["gt", "inferred"]
    return [get_same_group_annots(main_dir, kw) for kw in kwords]


def single_evaluate_main(main_dir, target_class):
    infer_gt, infer_model = get_inferred_and_gt_annots(main_dir)
    fscalc = FScoreCalculator(target_class)
    file_name = main_dir.name
    fscalc.evalaute(infer_gt, infer_model, file_name)


def multiple_thresh_evaluate_main(main_dir, btroubleshoot=False):
    inference_dir = main_dir / "inference_output"
    inf_parq_fnames = os.listdir(inference_dir)
    inf_parq_fnames.sort()
    inf_parq_paths = [inference_dir / x for x in inf_parq_fnames]
    img_dir = None
    if btroubleshoot:
        str_img_dir = str(main_dir).replace("parquet_files","image_annots_files")
        img_dir = Path(str_img_dir) / "images"
    df_cm_accumulator = {}
    for infer_path in inf_parq_paths:  # [-1:]:
        shutil.copy(infer_path, main_dir)
        new_inf_path = main_dir / infer_path.name
        threshold = "0." + infer_path.stem.split(".")[-1]
        print(threshold)
        infer_gt, infer_model = get_inferred_and_gt_annots(main_dir)
        fscalc = FScoreCalculator(target_class, img_dir)
        file_name = main_dir.name + f"{threshold}"
        if btroubleshoot: fscalc.inference_troubleshooter.create_displaying_detection_folders(threshold)
        df_cm_accumulator[threshold] = fscalc.evalaute(infer_gt, infer_model, file_name)
        os.remove(new_inf_path)

    print(df_cm_accumulator)


def multi_fps_evaluate_main(main_dir):
    # image_dir = Path("C:/Users/MalumAdmin/Downloads/annotations/lone_star_dec20_weapon/images")
    multiplier_frames = [1, 2, 3, 5, 10, 15]
    infer_gt, infer_model = get_inferred_and_gt_annots(main_dir)
    fscalc = FScoreCalculator(target_class)
    file_name = main_dir.name
    for multiplier_frame in multiplier_frames:
        print(f"{30 / multiplier_frame} fps evaluation")
        fscalc.evalaute_skipping_frames(infer_gt, infer_model, multiplier_frame, file_name)

from non_maxima_suppression import NonMaximaSupression
fr_inds = ["12-25-2022_fall_weapon_cellphones-9-_png_jpg.rf.b1d0587df34a04ba6d0800c24d5641d2.jpg",
           "12-25-2022_fall_weapon_cellphones-18-_png_jpg.rf.019063a90ad8e2215c25cc4077235b57.jpg",
           "12-25-2022_fall_weapon_cellphones-19-_png_jpg.rf.019063a90ad8e2215c25cc4077235b57.jpg"]
def display_specific_frame_detections(main_dir):
    global fr_inds
    nms = NonMaximaSupression()
    gt_df, infer_df = get_inferred_and_gt_annots(main_dir)
    col = "frame"
    for fr_ind in fr_inds:
        sel_inf_rows = infer_df[infer_df[col] == fr_ind]
        nms.remove_redundancy_on_inference(sel_inf_rows.values.tolist())
        sel_gt_rows = gt_df[gt_df[col] == fr_ind]
        print(sel_inf_rows, sel_gt_rows)
        a=1

if __name__ == "__main__":
    """
    multiple_thresh_evaluate_main and multi_fps_evaluate_main scripts expect all predictions and gt are recorded as table
    """
    target_class = class_labels_name.copy()#list(set(cls2centernet_dict.values()))
    main_dir = Path("C:/projects/data/annotations/parquet_files/test_weapon/generalized_weapon/roboflow_corrected")
    display_specific_frame_detections(main_dir)
    # multiple_thresh_evaluate_main(main_dir, btroubleshoot=False)
    # multi_fps_evaluate_main(main_dir)
