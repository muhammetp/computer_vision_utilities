import os.path

from yaml_annotation_parser import create_annotations_pickle
import cv2
import torch
import torchvision.transforms as transforms
from PIL import Image
import numpy as np
from detector_inference_util import predict, create_detector
import pickle

# input_path = "D:\\intellisee_data\\full_data\\falses\\feedback_2021-07-09_misc_added\\images"
# output_path = ".\\visual_bb"


class_dict = {'person_fallen': 0, 'person_sitting': 1, 'person_standing': 2, 'snow': 3, 'spill': 4, 'vehicle': 5,
              'weapon': 6, 'unknown': 7}

abbr_dict = {'person_fallen': 'PFN', 'person_sitting': 'PSI', 'person_standing': 'PST', 'snow': 'SN', 'spill': 'SP',
             'vehicle': 'VHC', 'weapon': 'WPN', 'unknown': 'UKN'}

cls2centernet_dict = {'person_fallen': 'person_on_ground', 'person_sitting': 'person1', 'person_standing': 'person',
                      'snow': 'slip-risk', 'spill': 'slip_risk', 'vehicle': 'vehicle', 'weapon': 'weapon',
                      'unknown': 'unknown', 'handgun': 'weapon'}

#class_id2str_dict = {v: k for k, v in class_dict.items()}
centernet2cls_dict = {v: k for k, v in cls2centernet_dict.items()}
centernet2int_dict = {k: i for k, i in zip(cls2centernet_dict.values(), range(len(cls2centernet_dict)))}

input_size = 224
test_transform = transforms.Compose([
    transforms.Resize(size=256),
    transforms.CenterCrop(size=input_size),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406],
                         [0.229, 0.224, 0.225])
])

def calculate_confusion_matrix(y_true, y_pred, classes=None, model_name=None):
    global class_dict
    fig_name = '.\\results\\confusion_matrix.png'
    if classes is None:
        union_cls = list(set(y_true).union(set(y_pred)))
        union_cls.sort()
        classes = [int2centernet_dict[x] for x in union_cls]
    if isinstance(y_pred[0], str):
        y_pred = [class_dict[x] for x in y_pred]
    cf_matrix = confusion_matrix(y_true, y_pred)
    print("confusion_matrix: \n", cf_matrix)
    df_cm = pd.DataFrame(cf_matrix, index=[i for i in classes],
                         columns=[i for i in classes])
    sums = df_cm.sum(axis=1)
    #df_cm = df_cm.div(sums, axis=0)
    print("total number for each class: \n", sums)
    plt.figure(figsize=(12, 7))
    sn.heatmap(df_cm, annot=True, fmt='g')
    if model_name is not None:
        splits = fig_name.rsplit(".",1)
        fig_name = splits[0] + "_" + model_name + "." + splits[1]
    plt.savefig(fig_name)
def image_loader(image):
    """load image, returns cuda tensor"""
    image = Image.fromarray(np.uint8(image)).convert('RGB')
    image = test_transform(image).float()
    image = image.unsqueeze(0)
    return image.cuda()


def execute_center_detection(path, model, img, label):
    center_preds = predict(path, model)
    # preds, scores, bboxes, = centernet_parser(center_preds)
    # draw_preds(img, preds, scores, bboxes, label, b_centernet=True)
    return centernet_parser(center_preds)


def execute_detected(_images, _bboxes, _image_prev, label):
    global model, model_center

    with torch.no_grad():
        output = model(_images)
        max_prob, max_ind = torch.max(output, 1)
        preds = [cls2centernet_dict[class_id2str_dict[x.item()]] for x in max_ind]

    max_prob = max_prob.cpu().data.numpy()
    draw_preds(_image_prev, preds, max_prob, _bboxes, label)
    return preds
    # execute_center_detection(_image_prev, label)


col_dict={"prev":(255, 0, 0), "recent":(0, 255, 0),"true":(0, 0, 255)}
def draw_results(_img, pred, model):
    global col_dict
    _preds, _scores, _bboxes = pred
    color = col_dict[model]
    for pred, score, bbox in zip(_preds, _scores, _bboxes):
        x1, y1, x2, y2 = bbox
        cv2.rectangle(_img, (x1, y1), (x2, y2), color, 2)
        cv2.imshow("images", cv2.resize(_img, (960, 540)))
        cv2.waitKey(2000)


def draw_all(img, _y_tr, _y_prev, _y_recent):
    draw_results(img, _y_tr, "true")
    draw_results(img, _y_prev, "prev")
    draw_results(img, _y_recent, "recent")


def draw_preds(_image, _preds, max_prob, _bboxes, labels, b_centernet=False):
    color = (0, 0, 255) if not b_centernet else (255, 0, 0)
    pix_step = 30 if not b_centernet else 90
    for pred, score, bbox, label in zip(_preds, max_prob, _bboxes, labels):
        x1, y1, x2, y2 = bbox
        texts = ["true label :" + label, " pred :" + pred + " score: " + str(round(score, 2))]
        if b_centernet:
            texts = ["centernet pred :" + pred + " score: " + str(round(score, 2))]
        y_cord = y2 + pix_step
        for text in texts:
            cv2.putText(img=_image, text=text, org=(x1, y_cord), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1,
                        color=color, thickness=2)
            y_cord += pix_step
        cv2.rectangle(_image, (x1, y1), (x2, y2), color, 2)
        cv2.imshow("images", cv2.resize(_image, (960, 540)))
        cv2.waitKey(1)
    cv2.waitKey(1000)


def centernet_parser(inp_center_preds):
    global weapon_labels
    preds = [];
    scores = [];
    bboxes = []
    for center_pr in inp_center_preds:
        pred, score, *bbox, = center_pr
        if pred in weapon_labels:
            preds.append(pred), scores.append(score), bboxes.append(bbox)

    return preds, scores, bboxes

def intersect_bboxes(bbox1, bbox2):
    x1, y1, x2, y2 = bbox1
    x_1, y_1, x_2, y_2 = bbox2
    tlx = max(x1, x_1)
    tly = max(y1, y_1)
    brx = min(x2, x_2)
    bry = min(y2, y_2)
    cond1, cond2 = (brx - tlx) > 0, (bry - tly) > 0

    if cond1 and cond2:
        return True

    return False


def search_box_in_allboxes(tar_bbxs, tar_labels, all_boxes, all_preds, fp_counter):
    for i, tr_bbx in enumerate(tar_bbxs):
        for j, bbx in enumerate(all_boxes):
            if intersect_bboxes(tr_bbx, bbx):
                all_preds[i] = tar_labels[i]
                del all_boxes[j]
                break
    for j, bbx in enumerate(all_boxes):
        bFP = True
        for i, tr_bbx in enumerate(tar_bbxs):
            if intersect_bboxes(tr_bbx, bbx):
                bFP = False
                break
        if bFP:
            print("box {} is detected as FP".format(bbx))
            fp_counter += 1
    return all_preds, fp_counter
def accumulator(y_tr, y_pr_prev, y_pr_rec):
    global y_true, y_prev, y_recent, centernet2int_dict, fp_prev, fp_recent

    tr_labels, prev_labels, recent_labels = y_tr[0], y_pr_prev[0], y_pr_rec[0]
    tr_bbxs, prev_bbxs, recent_bbxs = y_tr[2], y_pr_prev[2], y_pr_rec[2]

    uk_val = "unknown"
    labels = [centernet2int_dict[x] for x in tr_labels]
    y_p = [centernet2int_dict[uk_val] for x in tr_labels]
    y_r = [centernet2int_dict[uk_val] for x in tr_labels]

    y_p, fp_prev = search_box_in_allboxes(tr_bbxs, labels, prev_bbxs, y_p, fp_prev)
    y_r, fp_recent = search_box_in_allboxes(tr_bbxs, labels, recent_bbxs, y_r, fp_recent)

    y_true = y_true + labels
    y_prev = y_prev + y_p
    y_recent = y_recent + y_r
    y_prev

locations_folder = "C:/Users/mpak8/Downloads/test-dataset/"
filename = "../dct_integration/annot.pickle"
if not os.path.isfile(filename):
    annots = create_annotations_pickle(locations_folder, filename)
else:
    with open(filename, 'rb') as handle:
        annots = pickle.load(handle)
path_prev = annots[0][0]
images = None
image_prev = cv2.imread(path_prev)
bboxes = []

cv2.namedWindow("images", cv2.WINDOW_NORMAL)
labels = [];y_true = [];y_recent = [];y_prev = []
fp_recent = 0; fp_prev=0;
def main():
    model_prev = create_detector("centernet_1_28.pt")
    model_recent = create_detector("centernet_recent.pt")

    weapon_labels = ["weapon", "handgun"]
    for annot in annots:
        path, *bbox, label = annot
        label = cls2centernet_dict[label]
        if label not in weapon_labels:
            continue

        x1, y1, x2, y2 = [int(float(x) + 0.5) for x in bbox]
        x1, x2 = (x2, x1) if x1 > x2 else (x1, x2)
        y1, y2 = (y2, y1) if y1 > y2 else (y1, y2)
        img = cv2.imread(path)

        if img is None:
            break

        crop_img = img[y1:y2, x1:x2]
        cv2.imshow("crop_img", crop_img)
        img_torch = image_loader(crop_img)

        if path_prev == path:
            images = torch.clone(img_torch) if images is None else torch.cat((images, img_torch), 0)
            labels.append(label)
            bboxes.append((x1, y1, x2, y2))
            continue
        y_pr_prev = execute_center_detection(path_prev, model_prev, image_prev, labels)
        y_pr_rec = execute_center_detection(path_prev, model_recent, image_prev, labels)
        y_tr = labels, [-1]*len(labels), bboxes
        draw_all(image_prev, y_tr, y_pr_prev, y_pr_rec)
        try:
            accumulator(y_tr, y_pr_prev, y_pr_rec)
        except Exception as e:  # work on python 3.x
            print('Failed to upload to ftp: ' + str(e))
            print(path)
        image_prev = img.copy()
        path_prev = path
        images = torch.clone(img_torch)
        bboxes = [(x1, y1, x2, y2)]
        labels = [label]


    calculate_confusion_matrix(y_true, y_prev,None, "previous1.28")
    calculate_confusion_matrix(y_true, y_recent,None, "recent")
    print("fp_prev:{}".format(fp_prev))
    print("fp_recent:{}".format(fp_recent))


if __name__ == "__main__":
    main()
