from dataclasses import dataclass
import pyarrow.csv as pv
import pyarrow.parquet as pq
from pathlib import Path
import pandas as pd


@dataclass
class ReadHandler:
    @staticmethod
    def csv_read(filename):
        # with open(filename, 'r', newline='') as f:
        #     reader_obj = csv.reader(f)
        #     rows = [row for row in reader_obj]
        # print(rows)
        return pd.read_csv(filename)

    @staticmethod
    def parquet_read(filename):
        # df_parquet = pd.read_parquet(filename)
        # rows = df_parquet.values.tolist()
        # print(rows)
        return pd.read_parquet(filename)

    @staticmethod
    def convert_csv2parquet(filename):
        if isinstance(filename, Path):
            filename = filename.__str__()
        table = pv.read_csv(filename)
        pq.write_table(table, filename.replace('csv', 'parquet'))