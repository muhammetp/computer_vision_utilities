import os
import cv2
from pathlib import Path
from pascal_voc_writer import Writer
from dataclasses import dataclass
from model_evaluator_util_funcs import calculate_size_from_json, parse_box_and_label
import xmltodict
from detector_inference_util import write_table_into_file

@dataclass
class GroundTruthHandler:
    xml_dir_path: Path
    img_dir_path: Path
    img_size: tuple
    gt_detection_table: list

    def __init__(self,xml_dir_path: Path=None, json_dir_path: Path=None, im_size: tuple = None):
        if json_dir_path is not None:
            self.img_size = calculate_size_from_json(json_dir_path) if im_size is None else im_size
            self.xml_dir_path = json_dir_path.parent.resolve() / "xml"
            self.xml_dir_path.mkdir(parents=True, exist_ok=True)
        else:
            self.xml_dir_path = xml_dir_path
            self.img_dir_path = xml_dir_path.parent / "images"
        self.gt_detection_table = []
    def create_xml_file(self, img_path: Path, detections: tuple):
        if len(detections) < 1:
            return
        w, h = self.img_size
        img_name = img_path.name
        xml_path = self.xml_dir_path / (img_name + ".xml")
        writer = Writer(img_path, w, h)
        for detection in detections:
            pred, bbox = parse_box_and_label(detection)
            bbox = bbox[0] + bbox[1]
            writer.addObject(pred, *bbox)

        if writer is not None:
            writer.save(xml_path)
    def parse_voc_xml_detection(self, dict_data:dict, img_file_name, bRoboflow=False,b_display=False):
        detections = dict_data["object"]
        if b_display:
            wnd_name = "verify"
            cv2.namedWindow(wnd_name, cv2.WINDOW_NORMAL)
            cv2.resizeWindow(wnd_name, 1080, 640)
            img = cv2.imread(str(self.img_dir_path / img_file_name))
        if isinstance(detections, dict):
            detections = [detections]
        for det in detections:
            bbox = list(det["bndbox"].values())
            bbox = [int(float(x)+0.5) for x in bbox]
            if bRoboflow: bbox = [bbox[0], bbox[2], bbox[1], bbox[3]]
            class_name = det["name"]
            detection = (img_file_name, "ground_truth", class_name, -1, *bbox)
            self.gt_detection_table.append(detection)
            if b_display:
                res = cv2.rectangle(img, bbox[:2], bbox[2:], (0, 0, 255))
                res = cv2.putText(res, class_name, bbox[:2], cv2.FONT_HERSHEY_SIMPLEX, 1,
                                  (255, 0, 0), 2, cv2.LINE_AA)
        if b_display:
            cv2.imshow(wnd_name, res)
            cv2.waitKey(2000)
    def read_xml(self, xml_path:Path)->dict:
        with open(xml_path) as file:
            file_data = file.read()
            dict_data = xmltodict.parse(file_data)
        return dict_data
    def read_xml_files(self, bRoboflow=True, b_quantigo=False, file_extension=None):
        xml_files = os.listdir(self.xml_dir_path)
        xml_files.sort()
        xml_fpaths = [self.xml_dir_path / x for x in xml_files]
        for xml_fpath, xml_file in zip(xml_fpaths, xml_files):
            dict_data = self.read_xml(xml_fpath)
            img_filename = xml_file.rsplit(".",1)[0] # + ".png"
            if not b_quantigo:
                img_filename += file_extension
            self.parse_voc_xml_detection(dict_data["annotation"], img_filename,bRoboflow,b_display=False)
        parent_dir = self.xml_dir_path.parent
        file_name = parent_dir / (parent_dir.name + "_gt.parquet")
        write_table_into_file(self.gt_detection_table, file_name)

if __name__ == "__main__":
    main_dir = Path("C:/projects/data/annotations/image_annots_files/test_weapon/generalized_weapon/"
                   "roboflow_corrected/xml")
    xml_handler = GroundTruthHandler(xml_dir_path=main_dir)
    xml_handler.read_xml_files(bRoboflow=True, b_quantigo=False, file_extension=".jpg")