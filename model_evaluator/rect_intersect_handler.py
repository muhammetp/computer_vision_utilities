from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Union
from model_evaluator_util_funcs import correct_order_elements_of_bbox
class IIntersectHandler(ABC):
    @abstractmethod
    def calculate_iou(self):
        """ calculate intersect and union of two objects
        :return:  a value between 0.0 - 1.0
        """


@dataclass
class RectIntersectHandler(IIntersectHandler):

    @staticmethod
    def calculate_rect_area(bbox: list) -> Union[int, float]:
        x1, y1, x2, y2 = bbox
        return (x2 - x1) * (y2 - y1)

    def _sum_rectangle_areas(self, bbox1, bbox2) -> Union[int, float]:
        bbox1_area = self.calculate_rect_area(bbox1)
        bbox2_area = self.calculate_rect_area(bbox2)
        return bbox1_area + bbox2_area

    def _find_intersect_rectangle(self, bbox1, bbox2) -> list:
        x1, y1, x2, y2 = bbox1
        x_1, y_1, x_2, y_2 = bbox2
        tlx = max(x1, x_1)
        tly = max(y1, y_1)
        brx = min(x2, x_2)
        bry = min(y2, y_2)
        return tlx, tly, brx, bry

    def calculate_iou(self, bbox1: list, bbox2: list) -> float:
        tlx, tly, brx, bry = self._find_intersect_rectangle(bbox1, bbox2)
        # cond1, cond2 = brx < tlx, bry < tly
        #
        # if cond1 or cond2:
        #     return 0.0
        intersection_area = abs(brx - tlx) * abs(bry - tly)
        if intersection_area <= 0:
            return intersection_area
        union_area = self._sum_rectangle_areas(bbox1, bbox2) - intersection_area
        iou_val = float(intersection_area) / float(union_area)
        return iou_val
def test_iou_case():
    bbox = [[511, 41, 577, 76], [577, 35, 511, 76]]
    bbox = [correct_order_elements_of_bbox(x) for x in bbox]
    inter_hand = RectIntersectHandler()
    print(inter_hand.calculate_iou(*bbox))

if __name__ == "__main__":
    test_iou_case()