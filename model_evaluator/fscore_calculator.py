import seaborn as sns
import pandas as pd
from rect_intersect_handler import RectIntersectHandler
from typing import Union
from model_evaluator_util_funcs import correct_order_elements_of_bbox
from inference_troubleshooter import InferenceTroubleShooter
from detector_inference_util import class_labels_name
from non_maxima_suppression import NonMaximaSupression

class FScoreCalculator:
    _confusion_matrix: dict
    intersect_handler: RectIntersectHandler = None
    empty_fscore_dict: dict = {}
    initial_fscore_dict: dict = {}
    _default_class: str
    inference_troubleshooter: InferenceTroubleShooter = None
    nms:NonMaximaSupression=None
    btr_shoot: bool = False

    def __init__(self, target_class: list = None, img_dir=None):
        self._confusion_matrix = {}
        self.intersect_handler = RectIntersectHandler()
        self._default_class = "others"
        if target_class is None:
            target_class = class_labels_name.copy()
        target_class.append(self._default_class)
        self.target_class = target_class
        self.empty_fscore_dict = {class_name: {} for class_name in target_class}
        self.initial_fscore_dict = {"tp": 0, "fp": 0, "fn": 0}
        self.nms = NonMaximaSupression()
        if img_dir:
            self.btr_shoot = True
            self.inference_troubleshooter = InferenceTroubleShooter(img_dir)

    def create_confusion_matrix(self, models):
        global empty_fscore_dict
        confusion_matrix = {};
        for model in models:
            confusion_matrix[model] = self.empty_fscore_dict.copy()
            for class_name in self.target_class:
                confusion_matrix[model][class_name] = self.initial_fscore_dict.copy()
        self._confusion_matrix = confusion_matrix

    def _data_parser(self, data: list) -> Union[str, float, list]:
        _, _, label, score, *bbox = data
        return label, score, correct_order_elements_of_bbox(bbox)

    """ iou > 0.5 and y_pred = y_true """

    def _is_tp(self, gt_row, model_row) -> bool:
        y_true, score, bbox = gt_row;
        y_pred, score, bbox_pred = model_row
        result = False
        if y_pred == y_true and self.intersect_handler.calculate_iou(bbox, bbox_pred) > 0: result = True
        return result

    def remove_non_interested_detections(self, model_rows: list, model_name: str):
        reduced_rows = model_rows.copy()
        for i, model_row in enumerate(model_rows):
            model_row_parsed = self._data_parser(model_row)
            label = model_row_parsed[0]
            '''check label is outside of '''
            if label not in self.target_class:
                label = self._default_class
                self._confusion_matrix[model_name][label]["fp"] += 1
                reduced_rows.remove(model_row)
        return reduced_rows

    def _check_and_process_tp(self, gt_row_parsed, model_rows, true_label, model_name):
        b_gt_matched = False
        for model_row in model_rows[:]:
            model_row_parsed = self._data_parser(model_row)
            '''tp checking'''
            if self._is_tp(gt_row_parsed, model_row_parsed):
                self._confusion_matrix[model_name][true_label]["tp"] += 1
                model_rows.remove(model_row)
                b_gt_matched = True
                break
        return b_gt_matched

    def _check_and_process_fn(self, gt_row, FNs, true_label, model_name, bfound_in_preds):
        if not bfound_in_preds:
            self._confusion_matrix[model_name][true_label]["fn"] += 1
            FNs.append(gt_row)

    def _check_and_process_fp(self, model_rows, model_name):
        for model_row in model_rows:
            label, _, _ = self._data_parser(model_row)
            self._confusion_matrix[model_name][label]["fp"] += 1

    def calculate_tp_fp_fn(self, gt_rows, model_rows, model_name):
        gt_rows = gt_rows.values.tolist()
        model_rows = model_rows.values.tolist()
        model_rows = self.remove_non_interested_detections(model_rows, model_name)
        self.nms.remove_redundancy_on_inference(model_rows)
        model_preds_org = model_rows.copy()
        FNs = []
        for gt_row in gt_rows:
            gt_row_parsed = self._data_parser(gt_row)
            true_label = gt_row_parsed[0]
            if true_label not in self.target_class:
                continue
            '''TP & FN check'''
            bfound_in_preds = self._check_and_process_tp(gt_row_parsed, model_rows, true_label, model_name)
            self._check_and_process_fn(gt_row, FNs, true_label, model_name, bfound_in_preds)

        '''remaining predictions are FP'''
        self._check_and_process_fp(model_rows, model_name)


        return FNs, model_rows


    def calculate_precision_recal(self):
        cm = self._confusion_matrix.copy()
        for model in cm:
            model_tpfpfn = cm[model]
            for label in model_tpfpfn:
                tpfpfn_dict = model_tpfpfn[label]
                tpfpfn = [x for x in tpfpfn_dict.values()]
                tp_val, fp_val, fn_val = tpfpfn[0], tpfpfn[1], tpfpfn[2]
                tpfpfn_dict["precision"] = round(float(tp_val) / float(tp_val + fp_val + 0.000001), 2)
                tpfpfn_dict["recal"] = round(float(tp_val) / float(tp_val + fn_val + 0.000001), 2)
                fscore = 2 * (tpfpfn_dict["recal"] * tpfpfn_dict["precision"]) / (
                        tpfpfn_dict["recal"] + tpfpfn_dict["precision"] + 0.000001)
                tpfpfn_dict["fscore"] = round(fscore, 2)
                model_tpfpfn[label] = tpfpfn_dict
            cm[model] = model_tpfpfn
        self._confusion_matrix = cm.copy()

    def display_confusion_matrix(self, file_name):
        def create_sns_image(df_cm):
            df_sel_cm = df_cm.copy()  # [['precision', 'recal', 'fscore']]
            sns.set(rc={'figure.figsize': (14, 8)})
            sns_plot = sns.heatmap(df_sel_cm, annot=True, fmt=".2f")
            # pos = sns_plot.get_yticks()
            # pos = [x + 0.5 for x in pos]
            # sns_plot.set_yticks(pos)
            fig = sns_plot.get_figure()
            fig.savefig(f"{file_name}.png")

        cm = self._confusion_matrix.copy()
        cols = ["model", 'label', "tp", 'fp', 'fn', 'precision', 'recal', 'fscore']
        data = []
        for model in cm:
            model_tpfpfn = cm[model]
            for target_class in model_tpfpfn:
                data.append([model, target_class, *list(model_tpfpfn[target_class].values())])
        df_cm = pd.DataFrame(data, columns=cols)
        df_cm = df_cm.loc[(df_cm["label"] == "person") | (df_cm["label"] == "longgun") | (df_cm["label"] == "handgun") | (
                    df_cm["label"] == "person_on_ground")]  # | (df_cm["label"] == "cellphone")]
        df_cm = df_cm.set_index(cols[:2])
        df_cm.sort_index(axis=0)
        print(df_cm)
        create_sns_image(df_cm)
        return df_cm
    def write_images(self, FNs, FPs, model_name):
        self.inference_troubleshooter.save_image_into_ts_folder(FPs, model_name, bFP=True)
        self.inference_troubleshooter.save_image_into_ts_folder(FNs, model_name, bFP=False)
    # TODO headers = ["frame", "model", 'label', "score", 'x1', 'y1', 'x2', 'y2']

    def evalaute(self, infer_gt: pd.DataFrame, infer_model: pd.DataFrame, file_name: str = 'output'):
        model_names = infer_model["model"].unique()
        self.create_confusion_matrix(model_names)
        models = [infer_model[infer_model["model"] == model] for model in model_names]
        frame_indexes = infer_gt["frame"].unique()
        for frame_index in frame_indexes:
            gt_rows = infer_gt[infer_gt["frame"] == frame_index]
            for model, model_name in zip(models, model_names):
                model_rows = model[model["frame"] == frame_index]
                FNs, FPs = self.calculate_tp_fp_fn(gt_rows, model_rows, model_name)
                if self.btr_shoot: self.write_images(FNs, FPs, model_name)
        self.calculate_precision_recal()
        return self.display_confusion_matrix(file_name)

    def evalaute_skipping_frames(self, infer_gt: pd.DataFrame, infer_model: pd.DataFrame, frame_period: int,
                                 file_name: str = 'output'):
        model_names = infer_model["model"].unique()
        self.create_confusion_matrix(model_names)
        models = [infer_model[infer_model["model"] == model] for model in model_names]
        frame_indexes = infer_gt["frame"].unique()
        nframes = len(frame_indexes)
        for time_stamp in range(0, nframes, frame_period):
            frame_index = frame_indexes[time_stamp]
            gt_rows = infer_gt[infer_gt["frame"] == frame_index]
            for model, model_name in zip(models, model_names):
                model_rows = model[model["frame"] == frame_index]
                FNs, FPs = self.calculate_tp_fp_fn(gt_rows, model_rows, model_name)

        self.calculate_precision_recal()
        return self.display_confusion_matrix(f"{file_name}_skipping_{int(30 / frame_period)}fps")