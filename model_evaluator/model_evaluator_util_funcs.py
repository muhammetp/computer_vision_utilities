import json
from typing import Union, Optional
import os
from pathlib import Path
import cv2
import numpy as np



KEYWORDS = ["person", "weapon", "spill"]
label_mapping = {}




def correct_order_elements_of_bbox(bbox: list) -> list:
    x1, y1, x2, y2 = bbox
    if x1 > x2: x1, x2 = x2, x1
    if y1 > y2: y1, y2 = y2, y1
    return [x1, y1, x2, y2]


def parse_labels_obj_keys(objects: dict):
    global label_mapping
    for object in objects:
        label_mapping[object["key"]] = object["classTitle"]


def parse_box_and_label_of_video_annot(detections: list):
    global label_mapping, lvl5_to_lvl4_dict
    labels = []
    bboxes = []
    if isinstance(detections, dict): detections = [detections]
    for detection in detections:
        label = label_mapping[detection["objectKey"]]
        if label in lvl5_to_lvl4_dict:
            label = lvl5_to_lvl4_dict[label]
        labels.append(label)
        bbox = detection["geometry"]["points"]["exterior"]
        bboxes.append(bbox[0] + bbox[1])
    return labels, bboxes


def parse_box_and_label(detection: dict):
    label = detection["classTitle"]
    bbox = detection["points"]["exterior"]
    return label, bbox


def filter_detections(detections: tuple):
    global KEYWORDS
    filt_detect = []
    for detection in detections:
        label, bbox = parse_box_and_label(detection)

        if (KEYWORDS[0] in label) or KEYWORDS[1] == label:
            filt_detect.append(detection)
    return filt_detect


def filter_video_detections(detections: dict):
    global KEYWORDS
    filt_detect = {}
    for detection in detections:
        frame_id = detection["index"]
        filt_detect[frame_id] = []
        objects = detection["figures"]
        labels, bbox = parse_box_and_label_of_video_annot(objects)
        if isinstance(labels, str): labels = [labels]; bbox = [bbox]; objects = [objects]
        for label, object in zip(labels, objects):
            filt_detect[frame_id].append(object)
    return filt_detect


def parse_json_file(filepath: Path, bVideoAnnotFile=False) -> Union[list, tuple]:
    filter_func = filter_detections
    with open(filepath) as f:
        all_json_objs = json.load(f)
        detections = all_json_objs["objects"] if not bVideoAnnotFile else all_json_objs["frames"]
        video_size = all_json_objs["size"]
        video_size = video_size["width"], video_size["height"]
        if bVideoAnnotFile:
            parse_labels_obj_keys(all_json_objs["objects"])
            filter_func = filter_video_detections

    return detections, video_size


def calculate_size_from_json(json_dir) -> tuple:
    json_fname = os.listdir(json_dir)[0]
    json_fpath = json_dir / json_fname
    detections, video_size = parse_json_file(json_fpath)
    return video_size



