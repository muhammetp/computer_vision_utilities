import shutil
from pathlib import Path
import cv2
from video_creator import get_all_file_paths
from model_evaluator_util_funcs import parse_box_and_label, parse_json_file, parse_box_and_label_of_video_annot
from ground_truth_handler import GroundTruthHandler
import sys

lvl5_to_lvl4_dict = {"person_w/weapon": "person_standing", "person_laying": "person_fallen",
                     "person_carrying non-weapon": "person_standing", "person_holding non-weapon":"person_standing"}
gun_dicts = {"handgun_occluded":"handgun", "long-gun_occluded":"longgun"}
target_classes = ["person_standing","spill","handgun","longgun","person_sitting","snow","person_fallen","cellphone",
                  "backpack","broom","suitcase","vehicle","reflection"]
ALL_GUN_COMB = set()
def display_detections(img_fpath, detections: tuple):

    impath = img_fpath.__str__() if isinstance(img_fpath, Path) else img_fpath.copy()
    img = cv2.imread(impath)
    #print("\n",impath)
    for i, detection in enumerate(detections):
        label, bbox = parse_box_and_label(detection)
        if label is None:
            continue
        res = cv2.rectangle(img, bbox[0], bbox[1], (0, 0, 255))
        res = cv2.putText(res, label, bbox[0], cv2.FONT_HERSHEY_SIMPLEX, 1,
                          (255, 0, 0), 2, cv2.LINE_AA)
        #print(f"{label},",  end='')
    if len(detections)>0:
        cv2.imshow(f"detection", res)
        cv2.waitKey(500)

def get_detection_from_table(table, frame_ind, start_point):
    roi_detect = []
    for i, obj in enumerate(table[start_point:]):
        frame, model_name, class_name, score, *bbox = obj
        if frame< frame_ind: continue
        elif frame == frame_ind:
            roi_detect.append([class_name, bbox])
        else:
            return start_point+i, roi_detect

def display_video_annotations(detection_table:list, video_path:str):
    vcap = cv2.VideoCapture(video_path)
    frame_width = int(vcap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(vcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    FPS = int(vcap.get(cv2.CAP_PROP_FPS))
    frame_count = 0
    start_point = 0
    out = cv2.VideoWriter('outpy.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), FPS, (frame_width, frame_height))
    #_, _ = vcap.read()
    frame_count = 0
    while True:
        bread, img = vcap.read()
        if not bread: break

        start_point, detections = get_detection_from_table(detection_table, frame_count, start_point)
        frame_count += 1
        for detection in detections:
            label, bbox = detection
            res = cv2.rectangle(img, bbox[:2], bbox[2:], (0, 0, 255))
            res = cv2.putText(res, label, bbox[:2], cv2.FONT_HERSHEY_SIMPLEX, 1,
                              (255, 0, 0), 2, cv2.LINE_AA)
            res = cv2.putText(res, str(frame_count), (10, frame_height-20), cv2.FONT_HERSHEY_SIMPLEX, 1,
                              (255, 0, 0), 2, cv2.LINE_AA)
        out.write(res)
        # cv2.imshow(f"detection", res)
        # cv2.waitKey(200)
    print(frame_count)
    vcap.release()
    out.release()




def insert_detection_into_table(detections:tuple, table:list, frame:int, model_name:str = "ground_truth", bVideoAnnotFile=True):
    global lvl5_to_lvl4_dict
    parse_func = parse_box_and_label  if not bVideoAnnotFile  else parse_box_and_label_of_video_annot
    for frame in detections:
        detection = detections[frame]
        class_names, bboxes = parse_func(detection)
        [table.append((frame, model_name, class_name, -1, *bbox)) for class_name, bbox in zip(class_names, bboxes)]
def convert_labels(detections:list)-> bool:
    global lvl5_to_lvl4_dict, ALL_GUN_COMB
    for i, detection in enumerate(detections):
        label = detection["classTitle"]
        if label in lvl5_to_lvl4_dict:
            detections[i]["classTitle"] = lvl5_to_lvl4_dict[label]
        if label == "weapons_guns":
            detection_tag = detection["tags"]
            if len(detection_tag):
                return True
            label = detection_tag[0]["name"]
            if label not in ALL_GUN_COMB:
                ALL_GUN_COMB.add(label)
            if label in gun_dicts:
                label = gun_dicts[label]
            elif label not in gun_dicts.values():
                sys.exit(f"please check {label} then insert into gun_dicts")
            detections[i]["classTitle"] = label
    return True
def filter_detections(detections:list):
    global target_classes
    for detection in detections[:]:
        label = detection["classTitle"]
        if label not in target_classes:
            detections.remove(detection)
def create_empty_tag_folder(json_dir_path):
    empty_tag_dir_path = json_dir_path.parent.resolve() / "empty_tagname"
    empty_tag_dir_path.mkdir(parents=True, exist_ok=True)
    return empty_tag_dir_path.__str__()

if __name__ == "__main__":
    bVideoAnnotFile = False
    json_dir = Path("C:/Users/MalumAdmin/Downloads/LoneStar_frames/ann")
    empty_tag_dir_path = create_empty_tag_folder(json_dir)
    out_dir = json_dir.parent


    img_dir = out_dir / "img"
    json_fnames = get_all_file_paths(json_dir)
    xml_handler = GroundTruthHandler(json_dir_path=json_dir)
    detection_table = []
    for nframe, jfname in enumerate(json_fnames):
        json_fpath = json_dir / jfname
        img_fpath = img_dir / json_fpath.stem
        detections, video_size = parse_json_file(json_fpath, bVideoAnnotFile=bVideoAnnotFile)
        has_empty_tag = convert_labels(detections)
        if not has_empty_tag:
            filter_detections(detections)
            xml_handler.create_xml_file(img_fpath, detections)
        else:
            print(f"{json_fpath} has empty tag")
            shutil.move(json_fpath.__str__(), empty_tag_dir_path)
        #display_detections(img_fpath, detections)

    print(ALL_GUN_COMB)
