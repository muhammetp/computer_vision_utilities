from pathlib import Path

import pandas as pd

from utility import draw_predictions
import cv2


def file_name_ext_seperator(filename:str):
    return filename.rsplit(".",1)

def shrink_by_half_images(img_to_shrink, division=2):
    shape = img_to_shrink.shape[:2]
    shape = [int(x / division) for x in shape]
    shape.reverse()
    return cv2.resize(img_to_shrink, shape)


class InferenceTroubleShooter:
    image_dir: Path
    troubleshooting_dir: Path
    curr_save_img_dir_path: Path
    folder_names:list = ["FP", "FN"]
    gt_folder_name:str = "GT"
    def __init__(self, image_dir):
        self.image_dir = image_dir if isinstance(image_dir, Path) else Path(image_dir)
        self.troubleshooting_dir = self.image_dir.parent.resolve() / "troubleshooting"
        self.troubleshooting_dir.mkdir(parents=True, exist_ok=True)
        (self.troubleshooting_dir / self.gt_folder_name).mkdir(parents=True, exist_ok=True)

    def create_displaying_detection_folders(self, threshold: int = None):
        troubleshooting_dir = self.troubleshooting_dir
        if threshold:
            self.curr_save_img_dir_path = troubleshooting_dir / str(threshold)
            self.curr_save_img_dir_path.mkdir(parents=True, exist_ok=True)
        [(self.curr_save_img_dir_path / x).mkdir(parents=True, exist_ok=True) for x in self.folder_names]

    def _insert_objs_into_images(self, defined_objs:list = [], bFP:bool=True):
        if len(defined_objs) < 1:
            return None, None
        fpath = str(self.image_dir / defined_objs[0][0])
        orig_img_rgb = cv2.imread(fpath)
        class_idx = 3 if bFP else 6
        font_thickness = 3

        for row in defined_objs:
            class_name = row[2]
            bbox = row[-4:]
            score = float(row[3])
            orig_img_rgb = draw_predictions(orig_img_rgb, class_name, class_idx, bbox, score, font_thickness)
        return orig_img_rgb, defined_objs[0][0]

    def save_image_into_ts_folder(self, defined_objs, model_name, bFP=True):
        insrt_obj_img, filename = self._insert_objs_into_images(defined_objs, bFP)

        if insrt_obj_img is None:
            return
        img_rgb = shrink_by_half_images(insrt_obj_img)
        name, ext = file_name_ext_seperator(filename)
        target_dir = self.folder_names[0] if bFP else self.folder_names[1]
        save_img_path = str(self.curr_save_img_dir_path / target_dir / f"{name}_{model_name}.{ext}")
        cv2.imwrite(save_img_path, img_rgb)

    def display_bboxes(self, defined_objs, model_name, bFP=True):
        wnd_name = f"FPs" if bFP else f"FNs"
        wait_time = 2000 if bFP else 2000
        insrt_obj_img, filename = self._insert_objs_into_images(defined_objs, model_name, bFP)
        if insrt_obj_img is None:
            return
        img_rgb = shrink_by_half_images(insrt_obj_img)
        cv2.imshow(wnd_name, img_rgb)
        cv2.waitKey(wait_time)