import cv2
import os
from pathlib import Path
from dataclasses import dataclass


def get_real_file_path(img_dir):
    filenames = os.listdir(img_dir)
    for fname in filenames:
        fpath = os.path.join(img_dir, fname)
        if os.path.isfile(fpath): return fpath


def get_all_file_paths(img_dir):
    filenames = os.listdir(img_dir)
    filenames.sort()
    filepaths = [os.path.join(img_dir, fname) for fname in filenames]
    filepaths = [fpath for fpath in filepaths if os.path.isfile(fpath)]
    return filepaths

@dataclass
class VideoCreator:
    img_dir: Path
    vcap: cv2.VideoWriter
    fps: int

    def __init__(self, img_dir, fps=30):
        self.img_dir = img_dir if isinstance(img_dir, Path) else Path(img_dir)
        self.fps = fps
        self.vcap = self.create_video_writer()

    def create_video_writer(self):
        img_dir = self.img_dir
        out_dir = img_dir.parent.resolve()
        vid_name = img_dir.parent.name
        img_path = get_real_file_path(img_dir)
        img = cv2.imread(img_path)
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')  # ('M', 'J', 'P', 'G') for .avi
        out_path = os.path.join(out_dir, f'{vid_name}.mp4')
        return cv2.VideoWriter(out_path, fourcc, self.fps, (img.shape[1], img.shape[0]))

    def write_images_into_video(self):
        file_paths = get_all_file_paths(img_dir)
        vcap = self.vcap
        [vcap.write(cv2.imread(img_path)) for img_path in file_paths]
        vcap.release()


if __name__ == "__main__":
    img_dir = Path("C:/Users/mpak8/Downloads/quantigo/img")
    VideoCreator(img_dir).write_images_into_video()
