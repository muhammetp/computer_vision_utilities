'''
you can run this script as follows:
python split_train_val.py parent_folder_path
'''
import os
import shutil
from pathlib import Path
import  sys
import random

bCreateTestData = sys.argv[2] if len(sys.argv)>3 else False


def create_folders(inp_path: Path):
    par_path = inp_path.parent.absolute()
    out_tr_path = par_path / "train"
    out_val_path = par_path / "val"
    out_tr_path.mkdir(parents=True, exist_ok=True)
    out_val_path.mkdir(parents=True, exist_ok=True)
    return out_tr_path, out_val_path


def dataset_split(all_file_name_list: list, tr_data_length_ratio:float):
    random.shuffle(all_file_name_list)
    idx= int(len(all_file_name_list) * tr_data_length_ratio)
    return all_file_name_list[:idx], all_file_name_list[idx:]


def copy_files(file_list: list, folder_name:str, inp_path: Path, out_path: Path):
    full_path = out_path / folder_name
    full_path.mkdir(parents=True, exist_ok=True)
    [shutil.copy(inp_path / file_name, full_path / file_name) for file_name in file_list]



if __name__ == "__main__":
    inp_path = Path("C:/projects/data/full_size_master_cropped" if len(sys.argv)<2 else sys.argv[1])
    out_tr_path, out_val_path = create_folders(inp_path)

    folder_list = os.listdir(inp_path)
    for fold_name in folder_list:
        fold_path = inp_path / fold_name
        # todo delete 100
        all_file_name_list = os.listdir(fold_path)[:100]
        train_img_name, val_img_name = dataset_split(all_file_name_list, 0.9)
        copy_files(train_img_name, fold_name, fold_path, out_tr_path)
        copy_files(val_img_name, fold_name, fold_path, out_val_path)