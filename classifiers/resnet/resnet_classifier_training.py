import cv2
import torch
import os
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
import numpy as np
from PIL import Image
import os.path
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sn
import numpy as np
import pandas as pd
from scipy.stats import norm
import sys
from pathlib import Path

test_transform = transforms.Compose([
    transforms.Resize(size=256),
    transforms.CenterCrop(size=224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406],
                         [0.229, 0.224, 0.225])
])


class_dict = {'person_fallen': 0, 'person_sitting': 1, 'person_standing': 2, 'snow': 3, 'spill': 4, 'vehicle': 5,
              'weapon': 6, 'unknown': 7}
inv_class_dict = {v: k for k, v in class_dict.items()}
abbr_dict = {'person_fallen': 'PFN', 'person_sitting': 'PSI', 'person_standing': 'PST', 'snow': 'SN', 'spill': 'SP',
             'vehicle': 'VHC', 'weapon': 'WPN', 'unknown': 'UKN'}

class_id2str_dict = {v: k for k, v in class_dict.items()}
cls2centernet_dict = {'person_fallen': 'person_on_ground', 'person_sitting': 'person1', 'person_standing': 'person',
                      'snow': 'slip-risk', 'spill': 'slip_risk', 'vehicle': 'vehicle', 'weapon': 'weapon',
                      'unknown': 'unknown'}
centernet2int_dict = {k: i for k, i in zip(cls2centernet_dict.values(), range(len(cls2centernet_dict)))}
int2centernet_dict = {v: k for k, v in centernet2int_dict.items()}
IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]

model = None
def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


def find_classes(dir):
    classes = os.listdir(dir)
    classes.sort()
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx


def make_dataset(dir, class_to_idx):
    images = []
    for target in os.listdir(dir):
        d = os.path.join(dir, target)
        if not os.path.isdir(d):
            continue
        class_id = -1
        if target in class_to_idx:
            class_id = class_to_idx[target]
        for filename in os.listdir(d):
            if is_image_file(filename):
                path = '{0}/{1}'.format(target, filename)
                item = (path, class_id)
                images.append(item)

    return images


class ImageFolder(Dataset):
    def __init__(self, root, transform=None, target_transform=None, classes=None, class_to_idx=None):
        global class_dict
        if classes is None:
            classes, class_to_idx = (class_dict.keys(), class_dict)
        imgs = make_dataset(root, class_to_idx)

        self.root = root
        self.imgs = imgs
        self.classes = classes
        self.class_to_idx = class_to_idx
        self.transform = transform
        self.target_transform = target_transform

    def __getitem__(self, index):
        path, target = self.imgs[index]
        img = Image.open(os.path.join(self.root, path)).convert('RGB')
        if self.transform is not None:
            img = self.transform(img)
            target=torch.tensor(target)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.imgs)


def convert_model_data(model, data):
    dev = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(dev)
    inputs, labels = data  # this is what you had
    inputs, labels = inputs.cuda(), labels.cuda()
    data = data.to(dev)
    return model, data


def conv_tensor2list(labels, output, y_true, y_pred):
    y_pr=[]; y_tr=[]
    output = (torch.max(torch.exp(output), 1)[1]).data.cpu().numpy()
    y_pr.extend(output)  # Save Prediction
    y_pred = y_pred + y_pr
    labels = labels.data.cpu().numpy()
    y_tr.extend(labels)  # Save Truth
    y_true = y_true + y_tr

    return y_true, y_pred


def calculate_confusion_matrix(y_true, y_pred, classes=None, model_name=None):
    global class_dict
    fig_name = '.\\results\\confusion_matrix.png'
    if classes is None:
        union_cls = list(set(y_true).union(set(y_pred)))
        union_cls.sort()
        classes = [int2centernet_dict[x] for x in union_cls]
    if isinstance(y_pred[0], str):
        y_pred = [class_dict[x] for x in y_pred]
    cf_matrix = confusion_matrix(y_true, y_pred)
    print("confusion_matrix: \n", cf_matrix)
    df_cm = pd.DataFrame(cf_matrix, index=[i for i in classes],
                         columns=[i for i in classes])
    sums = df_cm.sum(axis=1)
    #df_cm = df_cm.div(sums, axis=0)
    print("total number for each class: \n", sums)
    plt.figure(figsize=(12, 7))
    sn.heatmap(df_cm, annot=True, fmt='g')
    if model_name is not None:
        splits = fig_name.rsplit(".",1)
        fig_name = splits[0] + "_" + model_name + "." + splits[1]
    plt.savefig(fig_name)


def make_predictions(model, dataloader, b_unknown=False):
    model.eval()
    y_pred_prob = []; y_true = []; y_pred = []; max_probs = []
    with torch.no_grad():
        for data, label in dataloader:
            data, label = data.cuda(), label.cuda()
            output = model(data)
            if b_unknown:
                y_pred_prob = y_pred_prob + output.tolist()
            else:
                max_prob, max_ind = torch.max(output, 1)
                y_true = y_true + label.tolist()
                y_pred = y_pred + max_ind.tolist()
                max_probs = max_probs + max_prob.tolist()

    return y_true, y_pred, max_probs, y_pred_prob


def calculate_thresh_vals_for_target(model, dataloader, class_to_idx_dict, b_unknown):
    '''
    take prob of preds
    calculate pred class
    create df including pred, true, max_prob
    calculate mean std of each class using max_prob
    '''
    file_name = 'target_class_threshs.csv' if not b_unknown else 'unknown_class_threshs.csv'

    y_true, y_pred, max_probs, y_pred_prob = make_predictions(model, dataloader, b_unknown)
    target_class = class_to_idx_dict.keys()
    if not b_unknown:
        df = pd.DataFrame()
        df["max_prob"] = max_probs
        df["pred"] = y_pred
        df["true"] = y_true

        df_acc = df[df["true"]==df["pred"]]
        accuracy = len(df_acc) / len(df) * 100
        print("accuracy on train/val data: %{}".format(accuracy))
    else:
        y_pred_prob = np.array(y_pred_prob)
        df = pd.DataFrame(y_pred_prob, columns=target_class)

    df_thresh = pd.DataFrame(columns=['mean', 'std'], index=target_class)
    for key, val in class_to_idx_dict.items():
        if not b_unknown:
            df_sel = df_acc.loc[df_acc["true"]==val]
            mean, std = df_sel["max_prob"].mean(), df_sel["max_prob"].std()
        else:
            mean, std = df[key].mean(), df[key].std()
        df_thresh.loc[df_thresh.index == key, ['mean', 'std']] = [mean, std]

    df_thresh.to_csv(".//results//"+file_name, header=df_thresh.columns)


def df_thresh_generator(bUnknown):
    file_name = 'target_class_threshs.csv' if not bUnknown else 'unknown_class_threshs.csv'
    with open("..//results//" + file_name) as f:
        df = pd.read_table(f, sep=',', header=0, index_col=0, names=['mean', 'std'],
                                lineterminator='\n')
    #print(df)
    return df


def print_unknowns_in_knows(y_pred_cat, str_label):
    len_uk_pred = len([x for x in y_pred_cat if x == "unknown"])
    len_pred_total = len(y_pred_cat)
    uk_percentage = round(100.0 * len_uk_pred / len_pred_total, 2)
    keyword = str_label
    print("unknown in", keyword, "as percentage: %", uk_percentage, " out of ", len_pred_total, " data")


def unknown_analysis(y_pred, bUnknown):
    expr = y_pred == "unknown"
    uk_expr = expr  # if bUnknown else ~expr
    not_uk_expr = ~uk_expr
    accuracy = len(y_pred[uk_expr]) / len(y_pred) * 100
    print("unknown percentange: %{:10.2f}".format(accuracy))
    print("\nout of total data: {}".format(len(y_pred)))
    if bUnknown:
        y_mispred = y_pred[not_uk_expr]
        df = pd.DataFrame(y_mispred, columns=["label"])
        print(df["label"].value_counts())

def decide_known_vs_unknown_with_prob(y_probs, y_labels, inv_map_dict, b_unknown=True):
    """
    N_know: prob_gaussian of known objects
    N_unknow: prob_gaussian of unknown objects

    if N_unknown > N_known:
        label = misc
    """

    df_known = df_thresh_generator(bUnknown=False)
    df_unknown = df_thresh_generator(bUnknown=True)
    if not isinstance(y_probs, np.ndarray):
        y_probs = np.asarray(y_probs)
        y_labels = np.asarray(y_labels)
    y_cat = ["None"] * len(y_labels)
    for row_uk, row_k in zip(df_unknown.itertuples(), df_known.itertuples()):
        if row_uk[0] == row_k[0]:
            mu_k, std_k = row_k[1:]
            mu_uk, std_uk = row_uk[1:]
            num_label = inv_map_dict[row_uk[0]]
            str_label = row_uk[0]
        else:
            return print("different formatted thresh files")
        inds = np.where(y_labels == num_label)[0]
        y_sel = [y_probs[x] for x in inds]
        if len(inds)<1:
            continue
        prob_uk = norm(mu_uk, std_uk).pdf(y_sel)
        prob_k = norm(mu_k, std_k).pdf(y_sel)
        if not str_label in ["spill", "weapon"]:
            y_pred_cat = [str_label if pr_k>=pr_uk else "unknown" for pr_uk, pr_k in zip(prob_uk, prob_k)]
        else:
            # prob_k = norm(mu_k, std_k/2).pdf(y_sel)
            # y_pred_cat = [str_label if pr_k >= pr_uk else "unknown" for pr_uk, pr_k in zip(prob_uk, prob_k)]
            dec_thresh = 0.8 #mu_k - std_k
            y_pred_cat = [str_label if y_pr > dec_thresh else "unknown" for y_pr in y_sel]
        if not b_unknown:
            print_unknowns_in_knows(y_pred_cat, str_label)
        for ind, y_pr in zip(inds,y_pred_cat):
            y_cat[ind] = y_pr
    #print(y_cat[:100])
    return np.asarray(y_cat)
def decide_known_vs_unknown(y_probs, y_labels, inv_map_dict, b_unknown=True):
    """
    N_know: prob_gaussian of known objects
    thresh = mu_known - 2 * std_known
    if score < thresh:
        label = misc
    """

    df_known = df_thresh_generator(bUnknown=False)
    if not isinstance(y_probs, np.ndarray):
        y_probs = np.asarray(y_probs)
        y_labels = np.asarray(y_labels)
    y_cat = ["None"] * len(y_labels)
    for row_k in df_known.itertuples():
        str_label = row_k[0]
        if str_label in inv_map_dict:
            mu_k, std_k = row_k[1:]
            num_label = inv_map_dict[str_label]
        else:
            return print("different formatted thresh files")
        inds = np.where(y_labels == num_label)[0]
        y_sel = [y_probs[x] for x in inds]
        if len(inds)<1:
            continue
        thresh = 0.45 if str_label not in ["spill","snow"] else 0.65
        # thresh = mu_k - 2.5 * std_k if str_label != "spill" else 0.7
        #         if str_label != "spill" and thresh<0.5:
        #     thresh = 0.5
        # print(str_label," : ", thresh)
        y_pred_cat = [str_label if y_pr >= thresh else "unknown" for y_pr in y_sel]
        if not b_unknown:
            print_unknowns_in_knows(y_pred_cat, str_label)
        for ind, y_pr in zip(inds,y_pred_cat):
            y_cat[ind] = y_pr
    #print(y_cat[:100])
    return np.asarray(y_cat)

"""
    out_dir_name = "eval_" + inp_img_fold_path.split("\\")[-1]
    parent = os.path.dirname(inp_img_fold_path)
    out_dir_path = os.path.join(parent, out_dir_name)
    fold_list = os.listdir(inp_img_fold_path)
    arr_ind = 0
    for fold in fold_list:
        fold_path = os.path.join(inp_img_fold_path, fold)
        if not os.path.isdir(fold_path):
            continue
        file_list = os.listdir(fold_path)
"""
def visual_evaluation_on_image(test_loader, y_preds, max_probs):
    global class_id2str_dict, abbr_dict
    inp_fold_path = test_loader.dataset.root
    if isinstance(y_preds[0], int):
        y_preds = [class_id2str_dict[x]for x in y_preds]
    y_preds = [abbr_dict[x] for x in y_preds]
    for img_path, y_pred, max_prob in zip(test_loader.dataset.imgs, y_preds, max_probs):
        fpath = os.path.join(inp_fold_path, img_path[0])
        img = cv2.imread(fpath)
        max_prob = round(max_prob, 2)
        text = y_pred + ": " + str(max_prob)
        cv2.putText(img=img, text=text, org=(1, 50), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.5,
                    color=(0, 0, 0), thickness=2)
        cv2.imwrite(fpath,img)
    # with torch.no_grad():
    #     for i, (images, labels) in enumerate(test_loader, 0):
    #         sample_fname, _ = test_loader.dataset.imgs[i]
    #         print(sample_fname)



def test_image(test_path):
    global class_dict, model, class_id2str_dict
    if not os.path.isdir(test_path):
        sys.exit('file not found')

    test_dataset = ImageFolder(test_path, transform=test_transform)  ##you can use this part
    n_sample = len(test_dataset)
    #print("{} sample data to be tested".format(n_sample))
    test_dataloader = DataLoader(test_dataset, batch_size=16, shuffle=False)  # you can use this part
    y_true, y_pred, max_probs, y_pred_prob = make_predictions(model, test_dataloader)
    # visual_evaluation_on_image(test_dataloader, y_pred, max_probs)
    y_pred = decide_known_vs_unknown(max_probs, y_pred, class_dict)
    y_pred = [cls2centernet_dict[x] for x in y_pred]
    # visual_evaluation_on_image(test_dataloader, y_pred, max_probs)
    # calculate_confusion_matrix(y_true, y_pred)
    return y_pred, max_probs, y_true, test_dataloader


def map_predictions(_full_crop_dict, data_path):
    ypred, _max_probs, ytrue, _test_data = test_image(data_path)
    ord_img = np.array([x[0].split("/")[-1] for x in _test_data.dataset.imgs])
    pred_dict = {}
    for key in _full_crop_dict.keys():
        files = _full_crop_dict[key]
        pred_dict[key] = []
        for file in files:
            ind = (np.where(ord_img == file))[0][0]
            pred_tuple = (ypred[ind], _max_probs[ind])
            pred_dict[key].append(pred_tuple)
    return pred_dict

''' on memory calculation'''
def image_loader(image):
    """convert PIL image, returns cuda tensor"""
    image = Image.fromarray(np.uint8(image)).convert('RGB')
    image = test_transform(image).float()
    image = image.unsqueeze(0)
    return image.cuda()
def init_model():
    global model
    print("cwd:{}".format(os.getcwd()))
    file_model_name = "../../model_data/classifier.pt"
    model = torch.load(file_model_name)
    print("model {} is loaded".format(file_model_name))


if __name__ == "__main__":
    init_model()
    data_dir = Path("C:/Users/mpak8/Downloads/test_images")
    img_names = os.listdir(data_dir)
    img_arr = []
    for fname in img_names:
        fpath = os.path.join(data_dir, fname)
        img = cv2.imread(fpath)
        pytorch_img = image_loader(img)
        img_arr.append(pytorch_img)
    torch_arr = torch.squeeze(torch.stack(img_arr))
    model.eval()
    with torch.no_grad():
        output = model(torch_arr)
        max_probs, max_inds = torch.max(output, 1)
    for fname, max_prob, max_ind in zip(img_names, max_probs, max_inds):
        print(fname,max_prob.item(), inv_class_dict[max_ind.item()])