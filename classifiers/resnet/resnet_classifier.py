'''in config batch size is maximum # of camera per gpu'''
import cv2
import torch
import os
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
import numpy as np
from PIL import Image
import os.path
import matplotlib.pyplot as plt
#from sklearn.metrics import confusion_matrix
import seaborn as sn
import numpy as np
import pandas as pd
from scipy.stats import norm
import sys
from pathlib import Path
import dataclasses
from pipeline.detection.detector import Detector, Detection
from pipeline.detection.types import PreprocessingFunction, TupleInt4, Rect
from pipeline.detection.model import Model
from typing import Optional, Sequence, Tuple, Dict, List

test_transform = transforms.Compose([
    transforms.Resize(size=256),
    transforms.CenterCrop(size=224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406],
                         [0.229, 0.224, 0.225])
])


class_dict = {'person_fallen': 0, 'person_sitting': 1, 'person_standing': 2, 'snow': 3, 'spill': 4, 'vehicle': 5,
              'weapon': 6, 'unknown': 7}
inv_class_dict = {v: k for k, v in class_dict.items()}
abbr_dict = {'person_fallen': 'PFN', 'person_sitting': 'PSI', 'person_standing': 'PST', 'snow': 'SN', 'spill': 'SP',
             'vehicle': 'VHC', 'weapon': 'WPN', 'unknown': 'UKN'}

class_id2str_dict = {v: k for k, v in class_dict.items()}
cls2centernet_dict = {'person_fallen': 'person_on_ground', 'person_sitting': 'person1', 'person_standing': 'person',
                      'snow': 'slip-risk', 'spill': 'slip_risk', 'vehicle': 'vehicle', 'weapon': 'weapon',
                      'unknown': 'unknown'}
centernet2int_dict = {k: i for k, i in zip(cls2centernet_dict.values(), range(len(cls2centernet_dict)))}
int2centernet_dict = {v: k for k, v in centernet2int_dict.items()}
IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


class ResnetClassifier(Detector):
    def __init__(self, classifier_config, gpu_device, gpu_memory_limit,
     detector_model_preprocessing: PreprocessingFunction=None,
                 detector_model: Model=None, score_threshold: float=None, iou_threshold: float=None,
                 max_boxes_per_class: int=None, max_detections_per_image: int=None,
                 class_labels: Sequence[str]=None) -> None:
        super().__init__(detector_model_preprocessing, detector_model, class_labels)
        self._config = classifier_config
        self._gpu_device = gpu_device
        self._gpu_memory_limit = gpu_memory_limit
        self.__score_threshold = score_threshold
        self.__max_boxes_per_class = max_boxes_per_class
        self.__max_detections_per_image = max_detections_per_image
        self.__model = self._create()

    def _decode(self):
        pass
    def _create(self):
        cwd = Path(os.getcwd())
        folder_name = cwd.name
        print(f"cwd:{cwd}")
        file_model_name = (cwd.parent if folder_name=='pipeline' else cwd.parent.parent) / "model_data/classifier.pt"
        model = torch.load(file_model_name, map_location=torch.device(f"cuda:{self._gpu_device}"))
        print("model {} is loaded".format(file_model_name))
        return model
    def profile_peak_memory_usage(self, temporary_files_directory: Path) -> int:
        torch.cuda.reset_peak_memory_stats(self.gpu_device)
        _ = self.detect_preprocessed(
            np.zeros(self.input_shape, dtype=np.float32),
            self.input_shape
        )
        memory_stats = torch.cuda.memory_stats(self.gpu_device)
        return 1.5 * memory_stats["allocated_bytes.all.peak"]
    def display_predictions(self,img_names, max_probs, max_inds):
        for fname, max_prob, max_ind in zip(img_names, max_probs, max_inds):
            print(fname, max_prob.item(), inv_class_dict[max_ind.item()])

    def convert_from_tensor_to_number_and_str(self, max_probs, max_inds):
        global inv_class_dict, cls2centernet_dict
        max_probs = [x.item() for x in max_probs]
        classifier_label = [inv_class_dict[y.item()] for y in max_inds]
        detector_label = [cls2centernet_dict[label] for label in classifier_label]
        return max_probs, detector_label
    def predict(self, data):
        if not isinstance(data, torch.Tensor):
            data = self._convert_data(data)
        self.__model.eval()
        with torch.no_grad():
            output = self.__model(data)
            max_probs, max_inds = torch.max(output, 1)

        return self.convert_from_tensor_to_number_and_str(max_probs, max_inds)

    ''' on memory calculation'''
    def _image_loader(self, image):
        """convert PIL image, returns cuda tensor"""
        image = Image.fromarray(np.uint8(image)).convert('RGB')
        image = test_transform(image).float()
        image = image.unsqueeze(0)
        return image.cuda(self._gpu_device)
    def _convert_data(self, data):
        img_arr = []
        for img in data:
            pytorch_img = self._image_loader(img)
            img_arr.append(pytorch_img)
        torch_arr = torch.stack(img_arr)
        torch_arr = torch.squeeze(torch_arr, 1)
        return torch_arr
    def _to_detections(self, bounding_boxes: torch.Tensor, scores: torch.Tensor,
                        classes: torch.Tensor) -> Tuple[Detection, ...]:

        detections: List[Detection] = []
        for bb, score, class_id in zip(bounding_boxes, scores, classes):
            detections.append(
                Detection(
                    bb,
                    float(score),
                    int(class_id),
                    self.class_labels[int(class_id)]
                )
            )
        return tuple(detections)

def convert_model_data(model, data):
    dev = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(dev)
    inputs, labels = data  # this is what you had
    inputs, labels = inputs.cuda(), labels.cuda()
    data = data.to(dev)
    return model, data


def conv_tensor2list(labels, output, y_true, y_pred):
    y_pr=[]; y_tr=[]
    output = (torch.max(torch.exp(output), 1)[1]).data.cpu().numpy()
    y_pr.extend(output)  # Save Prediction
    y_pred = y_pred + y_pr
    labels = labels.data.cpu().numpy()
    y_tr.extend(labels)  # Save Truth
    y_true = y_true + y_tr

    return y_true, y_pred


def calculate_confusion_matrix(y_true, y_pred, classes=None, model_name=None):
    global class_dict
    fig_name = '.\\results\\confusion_matrix.png'
    if classes is None:
        union_cls = list(set(y_true).union(set(y_pred)))
        union_cls.sort()
        classes = [int2centernet_dict[x] for x in union_cls]
    if isinstance(y_pred[0], str):
        y_pred = [class_dict[x] for x in y_pred]
    cf_matrix = confusion_matrix(y_true, y_pred)
    print("confusion_matrix: \n", cf_matrix)
    df_cm = pd.DataFrame(cf_matrix, index=[i for i in classes],
                         columns=[i for i in classes])
    sums = df_cm.sum(axis=1)
    #df_cm = df_cm.div(sums, axis=0)
    print("total number for each class: \n", sums)
    plt.figure(figsize=(12, 7))
    sn.heatmap(df_cm, annot=True, fmt='g')
    if model_name is not None:
        splits = fig_name.rsplit(".",1)
        fig_name = splits[0] + "_" + model_name + "." + splits[1]
    plt.savefig(fig_name)


def make_predictions(model, dataloader, b_unknown=False):
    model.eval()
    y_pred_prob = []; y_true = []; y_pred = []; max_probs = []
    with torch.no_grad():
        for data, label in dataloader:
            data, label = data.cuda(), label.cuda()
            output = model(data)
            if b_unknown:
                y_pred_prob = y_pred_prob + output.tolist()
            else:
                max_prob, max_ind = torch.max(output, 1)
                y_true = y_true + label.tolist()
                y_pred = y_pred + max_ind.tolist()
                max_probs = max_probs + max_prob.tolist()

    return y_true, y_pred, max_probs, y_pred_prob



def visual_evaluation_on_image(test_loader, y_preds, max_probs):
    global class_id2str_dict, abbr_dict
    inp_fold_path = test_loader.dataset.root
    if isinstance(y_preds[0], int):
        y_preds = [class_id2str_dict[x]for x in y_preds]
    y_preds = [abbr_dict[x] for x in y_preds]
    for img_path, y_pred, max_prob in zip(test_loader.dataset.imgs, y_preds, max_probs):
        fpath = os.path.join(inp_fold_path, img_path[0])
        img = cv2.imread(fpath)
        max_prob = round(max_prob, 2)
        text = y_pred + ": " + str(max_prob)
        cv2.putText(img=img, text=text, org=(1, 50), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.5,
                    color=(0, 0, 0), thickness=2)
        cv2.imwrite(fpath,img)
    # with torch.no_grad():
    #     for i, (images, labels) in enumerate(test_loader, 0):
    #         sample_fname, _ = test_loader.dataset.imgs[i]
    #         print(sample_fname)



def test_image(test_path):
    global class_dict, model, class_id2str_dict
    if not os.path.isdir(test_path):
        sys.exit('file not found')

    test_dataset = ImageFolder(test_path, transform=test_transform)  ##you can use this part
    n_sample = len(test_dataset)
    #print("{} sample data to be tested".format(n_sample))
    test_dataloader = DataLoader(test_dataset, batch_size=16, shuffle=False)  # you can use this part
    y_true, y_pred, max_probs, y_pred_prob = make_predictions(model, test_dataloader)
    # visual_evaluation_on_image(test_dataloader, y_pred, max_probs)
    # y_pred = decide_known_vs_unknown(max_probs, y_pred, class_dict)
    y_pred = [cls2centernet_dict[x] for x in y_pred]
    # visual_evaluation_on_image(test_dataloader, y_pred, max_probs)
    # calculate_confusion_matrix(y_true, y_pred)
    return y_pred, max_probs, y_true, test_dataloader







if __name__ == "__main__":
    clf = ResnetClassifier()
    data_dir = Path("C:/Users/mpak8/Downloads/test_images")
    img_names = os.listdir(data_dir)
    img_arr = []
    for fname in img_names:
        fpath = os.path.join(data_dir, fname)
        img = cv2.imread(fpath)
        img_arr.append(img)
    max_probs, max_inds = clf.predict(img_arr)
    clf.display_predictions(img_names, max_probs, max_inds)
    # for fname, max_prob, max_ind in zip(img_names, max_probs, max_inds):
    #     print(fname,max_prob.item(), inv_class_dict[max_ind.item()])