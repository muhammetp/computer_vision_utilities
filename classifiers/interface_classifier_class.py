from abc import ABC, abstractmethod
from torchvision import datasets, models, transforms
from torch.utils.data import DataLoader
from dataclasses import dataclass
@dataclass
class ITrainingClassifier(ABC):
    tr_img_transform: transforms.Compose
    val_img_transform: transforms.Compose
    tr_data: datasets.ImageFolder
    val_data: datasets.ImageFolder
    tr_data_loader: DataLoader
    val_data_loader: DataLoader
    model: None
    bLogSoftmax: bool
    file_model_name: str

    @property
    @abstractmethod
    def iterate_tr_val_data(self):
        """
        calculate loss values, accuracy and backpropagation for training
        """

    @abstractmethod
    def evaluate_tr_val_results(self):
        """
        calculate avg loss and accuracy and select the best lost
        """
    @abstractmethod
    def train_and_validate(self):
        """
        train and valdiation process are performed
        """