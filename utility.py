import colorsys
import cv2
import numpy as np
# Generate colors for drawing bounding boxes.
hsv_tuples = [(x / 50, 1., 1.)
              for x in range(50)]
colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
colors = list(
    map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
        colors))

font = cv2.FONT_HERSHEY_SIMPLEX
font_scale = 0.75
font_color = (0, 0, 0)


def draw_predictions(orig_img_rgb, class_name, class_idx, bbox, score=0, font_thickness=2, model_name=None):
    global font, font_scale, colors, font_color
    x1, y1, x2, y2 = bbox
    label = f'{class_name} {score:.2f}' if score > 0 else f'{class_name}'
    label_size, baseline = cv2.getTextSize(label, font, font_scale, font_thickness)

    if y1 - label_size[1] >= 0:
        text_origin = np.array([x1, y1 - label_size[1] - baseline])
    else:
        text_origin = np.array([x1, y1 + 1])

    for i in range(font_thickness):
        cv2.rectangle(orig_img_rgb, (x1 + i, y1 + i), (x2 - i, y2 - i), color=colors[class_idx])

    cv2.rectangle(orig_img_rgb, tuple(text_origin), tuple(text_origin + label_size + (0, baseline)),
                  color=colors[class_idx], thickness=cv2.FILLED)
    cv2.putText(
        orig_img_rgb,
        label,
        (text_origin[0], text_origin[1] + label_size[1]),
        font,
        font_scale,
        font_color, font_thickness
    )

    if model_name:
        (x1, y1) = (50, 20);
        (x2, y2) = (x1 + 600, y1 + 100)
        cv2.rectangle(orig_img_rgb, (x1, y1), (x2, y2), color=(255, 255, 255), thickness=cv2.FILLED)
        cv2.putText(orig_img_rgb, model_name, (x1, y1 + 60), font, font_scale * 3, font_color, font_thickness * 3)
    return orig_img_rgb