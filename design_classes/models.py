from abc import ABC

class model_inference_handler(ABC):
    def init_model(self):
        pass
    def train(self):
        pass
    def preproccesing(self):
        pass
    def predict(self, batch: list):
        pass


class yolo(model_inference_handler):
    def init_model(self):
        "implement it"
    def train(self):
        "implement it"
    def preproccesing(self):
        "implement it"
    def predict(self, batch: list):
        "implement it"


class resnet(model_inference_handler):
    def init_model(self):
        "implement it"
    def train(self):
        "implement it"
    def preproccesing(self):
        "implement it"
    def predict(self, batch: list):
        "implement it"


class centernet(model_inference_handler):
    def init_model(self):
        "implement it"
    def train(self):
        "implement it"
    def preproccesing(self):
        "implement it"
    def predict(self, batch: list):
        "implement it"