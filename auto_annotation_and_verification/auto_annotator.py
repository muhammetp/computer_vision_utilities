import cv2
from pathlib import Path
from annotation_verifier import cls2centernet_dict
from annotation_xml_file_generator import AnnotationXMLFileGenerator
from annotation_verifier import AnnotationVerifier

#todo draw_predictions in detector_inference_util must be checked

class_dict = {'person_fallen': 0, 'person_sitting': 1, 'person_standing': 2, 'snow': 3, 'spill': 4, 'vehicle': 5,
              'weapon': 6, 'unknown': 7}

abbr_dict = {'person_fallen': 'PFN', 'person_sitting': 'PSI', 'person_standing': 'PST', 'snow': 'SN', 'spill': 'SP',
             'vehicle': 'VHC', 'weapon': 'WPN', 'unknown': 'UKN'}


target_class = class_dict.keys()

class_id2str_dict = {v: k for k, v in class_dict.items()}
centernet2cls_dict = {v: k for k, v in cls2centernet_dict.items()}
centernet2int_dict = {k: i for k, i in zip(cls2centernet_dict.values(), range(len(cls2centernet_dict)))}



counter = 0


def image_writer(_image):
    global counter
    counter += 1
    filename = "image{:03d}.jpeg".format(counter)
    cv2.imwrite(filename, _image)




def accumulator(tr_labels, clf_labels, cent_labels):
    global y_true, y_center, y_clfs, centernet2int_dict
    for lab, y_c, y_cen in zip(tr_labels, clf_labels, cent_labels):
        y_true.append(centernet2int_dict[lab])
        y_clfs.append(centernet2int_dict[y_c])
        y_center.append(centernet2int_dict[y_cen])


'''just puts parent direction where your image folder name located'''

if __name__ == '__main__':
    main_dir = Path('C:/Users/MalumAdmin/Downloads/longgun-handgun-person-generalization')
    img_dir_path = main_dir /'images' #"D:/intellisee_data/model_eval/broadlawn_test/quantigo_guns_cleaned" #"D:/intellisee_data/guns_google/images"  # sys.argv[1]
    bgenerate_annotations = True
    if bgenerate_annotations:
        not_annot_dir_path, xml_dir_path = AnnotationXMLFileGenerator(img_dir_path).xml_annotation_generator(b_draw=True)

    AnnotationVerifier(img_dir_path).verify_annotations()