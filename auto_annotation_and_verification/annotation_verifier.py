import cv2
import os
from pathlib import Path
import xmltodict
from annotation_xml_file_generator import AnnotationXMLFileGenerator
from annotation_util_functions import draw_preds
from typing import Tuple
from rectangle_handler import RectangleHandler
#from common_functions import draw_preds, cv_wnd_name


cls2centernet_dict = {'person_fallen': 'person_on_ground', 'person_sitting': 'person1', 'person_standing': 'person',
                      'snow': 'slip-risk', 'spill': 'slip_risk', 'vehicle': 'vehicle', 'weapon': 'weapon',
                      'unknown': 'unknown', 'handgun': 'weapon', 'backpack': 'backpack', 'suitcase':'suitcase'}


def parse_and_accumulate(detect_objs: list):
    bboxes = [];
    scores = [];
    preds = []
    if not isinstance(detect_objs, list):
        detect_objs = [detect_objs]
    for obj in detect_objs:
        bbox = [int(float(cord) + 0.5) for cord in obj['bndbox'].values()]
        class_name = obj['name']
        if class_name not in cls2centernet_dict.values():
            class_name = cls2centernet_dict[class_name]
        bboxes.append(bbox)
        preds.append(class_name)
        scores.append(-1.0)
    return bboxes, scores, preds


class AnnotationVerifier:
    annotation_file_handler: AnnotationXMLFileGenerator
    wnd_name: str
    wnd_size: Tuple[int, int]
    rect_handler: RectangleHandler
    def __init__(self, image_path):
        self.annotation_file_handler = AnnotationXMLFileGenerator(image_path)
        self.wnd_name = "verifier"
        self.wnd_size = (960, 540)
        self.rect_handler = RectangleHandler(self.wnd_name, self.wnd_size)

    def verify_annotations(self):
        xml_dir_path = Path(self.annotation_file_handler.xml_dir_path)
        img_dir_path = xml_dir_path.parent / "images"
        not_annot_dir_path = Path(self.annotation_file_handler.not_annot_dir_path)

        xml_file_list = os.listdir(xml_dir_path)
        for xml_path in xml_file_list:
            filename = xml_path.rsplit(".",1)[0] + ".png"
            xml_path = os.path.join(xml_dir_path, xml_path)
            with open(xml_path) as file:
                file_data = file.read()  # read file contents

            # parse data using package
            dict_data = xmltodict.parse(file_data)['annotation']
            filename = dict_data['filename']
            print(filename)
            filepath = (img_dir_path / filename).__str__()
            img = cv2.imread(filepath)

            detect_objs = dict_data['object']
            bboxes, scores, preds = parse_and_accumulate(detect_objs)
            key = draw_preds(self.wnd_name, img, preds, scores, bboxes)
            bboxes, preds = self.update_pred_boxes(key, img, preds, bboxes)
            self.annotation_file_handler.update_xml_file(filepath, filename, preds, bboxes)

    def update_pred_boxes(self, key, _image, _preds, _bboxes):
        org_img = _image.copy()
        if key == 32:
            cv2.imshow(self.wnd_name, cv2.resize(_image, self.wnd_size))
            print("press 'u' to update and 'd' to delete")
            key1 = cv2.waitKey(-1)
            if key1 == ord('u'):
                print("please 2 times left click to create a new rectangle on existing rectangles or "
                      "missing target object")
                self.rect_handler.update_rectangle(org_img, _bboxes, _preds)
            elif key1 == ord('d'):
                print("please right click on existing rectangle to delete")
                self.rect_handler.delete_rectangle(org_img, _bboxes, _preds)

        return _bboxes, _preds