import os
import shutil
import cv2
from pascal_voc_writer import Writer
from pathlib import Path
from detector_inference_util import predict
from annotation_util_functions import draw_preds





def centernet_parser(inp_center_preds):
    preds = [];
    scores = [];
    bboxes = []
    for center_pr in inp_center_preds:
        pred, score, *bbox, = center_pr
        preds.append(pred), scores.append(score), bboxes.append(tuple(bbox))

    return preds, scores, bboxes


def execute_center_detection(path):
    center_preds = predict(path)
    return centernet_parser(center_preds)


class AnnotationXMLFileGenerator:
    xml_dir_path: Path
    not_annot_dir_path: Path
    img_dir_path: Path

    def __init__(self, img_dir_path):
        parent_path = img_dir_path.parent.absolute()
        self.xml_dir_path = parent_path / "xml"
        self.not_annot_dir_path  = parent_path / "not_annotated"
        Path(self.xml_dir_path).mkdir(parents=True, exist_ok=True)
        Path(self.not_annot_dir_path).mkdir(parents=True, exist_ok=True)
        self.img_dir_path = img_dir_path

    def create_xml_file(self, img_path, img_name, _preds, _bboxes):
        xml_path = os.path.join(self.xml_dir_path, img_name.split(".")[0]) + ".xml"
        img = cv2.imread(img_path)
        h, w = img.shape[:2]
        writer = Writer(img_path, w, h)
        for pred, bbox in zip(_preds, _bboxes):
            writer.addObject(pred, *bbox)

        writer.save(xml_path)

    def update_xml_file(self, img_path, img_name, preds, bboxes):
        """move img into not annotated when xml is deleted"""

        writer = None
        xml_path = os.path.join(self.xml_dir_path, img_name.split(".")[0]) + ".xml"
        if len(preds) < 1:
            os.remove(xml_path)
            shutil.move(img_path, self.not_annot_dir_path)
            return writer
        return self.create_xml_file(img_path, img_name, preds, bboxes)

    def xml_annotation_generator(self, b_draw=False):
        img_dir_path = self.img_dir_path
        img_dir_path = Path(img_dir_path)
        img_paths = os.listdir(img_dir_path)
        for img_name in img_paths:
            img_path = os.path.join(img_dir_path, img_name)
            preds, scores, bboxes = execute_center_detection(img_path)
            if len(preds) < 1:
                shutil.move(img_path, self.not_annot_dir_path)
                continue

            self.create_xml_file(img_path, img_name, preds, bboxes)
            if b_draw:
                img = cv2.imread(img_path)
                draw_preds("xml_generator",img, preds, scores, bboxes)

        return self.not_annot_dir_path, self.xml_dir_path

