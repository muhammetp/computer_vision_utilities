from model_evaluator.rect_intersect_handler import RectIntersectHandler
from typing import Tuple, List
import cv2

def contains(rect, point):
    return ((rect[0] <= point[0]) & (rect[2] >= point[0])) and ((rect[1] <= point[1]) & (rect[3] >= point[1]))


def sort_rectangle_coordinates(coords):
    if len(coords)>1:
        if coords[0][0] > coords[1][0]:
            coords[0][0], coords[1][0] = coords[1][0], coords[0][0]
        if coords[0][1] > coords[1][1]:
            coords[0][1], coords[1][1] = coords[1][1], coords[0][1]
    return coords



class RectangleHandler:
    rightclck: Tuple[int, int]
    leftclck: List[Tuple]
    wnd_name: str
    wnd_size: Tuple[int, int]
    intersect_handler: RectIntersectHandler
    def __init__(self, wnd_name, wnd_size):
        self.wnd_size = wnd_size
        self.wnd_name = wnd_name
        self.intersect_handler = RectIntersectHandler()
        self.rightclck = None
        self.leftclck = []
        cv2.namedWindow(wnd_name, cv2.WINDOW_NORMAL)
        cv2.setMouseCallback(self.wnd_name, self.mouse_click)

    def mouse_click(self, event, x, y, flags, param):
        if event == cv2.EVENT_RBUTTONDOWN:
            self.rightclck = (x, y)
        elif event == cv2.EVENT_LBUTTONDOWN:
            if len(self.leftclck) > 1:
                self.leftclck = []
            self.leftclck.append((x, y))
    def convert_coord(self, coords, img):
        h, w = img.shape[:2]
        if not isinstance(coords, list):
            coords = [coords]
        for i, coord in enumerate(coords):
            x, y = coord
            x = int(float(x) * (float(w) / self.wnd_size[0]) + 0.5)
            y = int(float(y) * (float(h) / self.wnd_size[1]) + 0.5)
            coords[i] = [x,y]
        return sort_rectangle_coordinates(coords)

    def select_rectangles_to_delete(self, inp_img, inp_bboxes):
        self.rightclck = None
        while self.rightclck is None:
            print("please right click on one of existing rectangles on images to delete")
            cv2.waitKey(2000)
        self.rightclck = self.convert_coord(self.rightclck, inp_img)[0]
        index = None
        rcolor = (0, 0, 255)
        bcolor = (255, 0, 0)
        org_img = inp_img.copy()
        for i, bbox in enumerate(inp_bboxes):
            x1, y1, x2, y2 = bbox
            if contains(bbox, self.rightclck):
                cv2.rectangle(inp_img, (x1, y1), (x2, y2), bcolor, 2)
                index = i
            else:
                cv2.rectangle(inp_img, (x1, y1), (x2, y2), rcolor, 2)
        cv2.imshow(self.wnd_name, cv2.resize(inp_img, self.wnd_size))
        print("please hit 'o' key to verify or 'r' key to retry or any key to continue")
        key = cv2.waitKey(-1)
        return index, key if key != ord('r') else self.select_rectangles_to_delete(org_img, inp_bboxes)

    def select_rectangles_to_update(self, inp_img, inp_bboxes):

        self.leftclck = []
        while len(self.leftclck)<2:
            print("please left clicks two times on one of existing rectangles or new rectangles on images to update/add new")
            cv2.waitKey(1000)
        leftclck = self.convert_coord(self.leftclck, inp_img)
        sel_bbox =[leftclck[0][0], leftclck[0][1], leftclck[1][0], leftclck[1][1]]
        self.leftclck = leftclck
        index = None
        rcolor = (0, 0, 255); bcolor = (255, 0, 0)
        org_img = inp_img.copy()
        for i, bbox in enumerate(inp_bboxes):
            x1, y1, x2, y2 = bbox
            if self.intersect_handler.calculate_iou(bbox, sel_bbox) > 0.1:
                index = i
            cv2.rectangle(inp_img, (x1, y1), (x2, y2), rcolor, 2)
        cv2.rectangle(inp_img, (sel_bbox[:2]), (sel_bbox[2:]), bcolor, 2)
        cv2.imshow(self.wnd_name, cv2.resize(inp_img, self.wnd_size))
        print("please hit 'o' key to verify or 'r' key to retry or any key to continue")
        key = cv2.waitKey(-1)
        return index, key if key != ord('r') else self.select_rectangles_to_delete(org_img, inp_bboxes)

    def delete_rectangle(self, img, bboxes, preds):
        index, key = self.select_rectangles_to_delete(img, bboxes)
        if index is not None and key == ord('o'):
            del bboxes[index]
            del preds[index]
        else:
            print("continues to verification")
        return bboxes, preds

    def update_rectangle(self, img, bboxes, preds):
        leftclck = self.leftclck
        index, key = self.select_rectangles_to_update(img, bboxes)
        sel_box = [leftclck[0][0], leftclck[0][1], leftclck[1][0], leftclck[1][1]]
        if index is not None and key == ord('o'):
            bboxes[index] = sel_box
        elif index is None and key == ord('o'):
            bboxes.append(sel_box)
            preds.append(preds[0])
        else:
            print("continues to verification")
        return bboxes, preds
