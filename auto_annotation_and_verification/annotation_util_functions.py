import cv2


def draw_preds(wnd_name,img, preds, scores, bboxes):
    for pred, score, bbox in zip(preds, scores, bboxes):
        cv2.rectangle(img, bbox[:2], bbox[2:], (0, 0, 255))
        # font
        font = cv2.FONT_HERSHEY_SIMPLEX

        # org
        org = bbox[2:]

        # fontScale
        fontScale = 1

        # Blue color in BGR
        color = (255, 0, 0)

        # Line thickness of 2 px
        thickness = 2
        image = cv2.putText(img, pred, org, font,
                            fontScale, color, thickness, cv2.LINE_AA)
    cv2.imshow(wnd_name, img)
    return cv2.waitKey(2000)