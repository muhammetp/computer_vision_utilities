import cv2
from pathlib import Path
import os
import numpy as pd


video_dir = Path("C:/Users/mpak8/Downloads/out_videos_wo_cvtcolor/Weapon - Cam1.69 - 6 October 2022.mp4")
out_dir = video_dir.parent.parent.absolute() / "frame_extraction"
out_dir.mkdir(parents=True, exist_ok=True)
vcap = cv2.VideoCapture(video_dir.__str__())
nframe = 0
success,_ = vcap.read()

while success:
    success, image = vcap.read()
    if success: nframe += 1
    #if not ((nframe+15)%30 == 1): continue
    vpath = os.path.join(out_dir,f"frame{nframe+1}.jpg")
    cv2.imwrite(vpath, image)

