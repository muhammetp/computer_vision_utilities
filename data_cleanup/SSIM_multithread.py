import os.path
import numpy as np
import cv2
from skimage.metrics import structural_similarity as compare_ssim
import os.path
import numpy as np
import cv2
import random
import argparse
import os
from pathlib import Path
import shutil
import matplotlib.pyplot as plt
import time
from threading import Thread
import multiprocessing
from multiprocessing import Manager

_start_time = time.time()
def tic():
    global _start_time
    _start_time = time.time()

def tac():
    t_sec = round(time.time() - _start_time)
    (t_min, t_sec) = divmod(t_sec, 60)
    (t_hour, t_min) = divmod(t_min, 60)
    print('Time passed: {}hour:{}min:{}sec'.format(t_hour, t_min, t_sec))

def split(a, n):

    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

# threads count (number of logical cores -1)
threads_count = int((multiprocessing.cpu_count() - 1)/2)
print("{} threads used".format(threads_count))

def write_image(fname, img):
    global fold_path
    fpath = os.path.join(fold_path, fname+".jpeg")
    cv2.imwrite(fpath, img)


def plot_hist(data, suffix=''):
    plt.hist(data, bins=10)
    plt.savefig('SSIM_score{}.png'.format(suffix))
    plt.clf()


def calc_ssim(data:np.ndarray):
    base_img = data[...,0]
    data = data[...,1:]
    n_sample = data.shape[-1]
    return np.asarray([compare_ssim(base_img, data[..., i]) for i in range(n_sample)]), data


def write_files_into_folder(init_path, acc, fold_path, relation):
    rel_path = os.path.join(fold_path, "rel{}".format(relation))
    Path(rel_path).mkdir(parents=True, exist_ok=True)
    init_path = [os.path.join(rel_path, x) for x in init_path]
    [cv2.imwrite(fpath, img) for fpath, img in zip(init_path, acc)]


def move_files(data_paths, dst_par_path, dst_fold_name):
    dst_fold_path = os.path.join(dst_par_path, dst_fold_name)
    Path(dst_fold_path).mkdir(parents=True, exist_ok=True)
    [shutil.move(fpath, dst_fold_path) for fpath in data_paths]


def accumulate_images(file_paths):
    img_size = (256, 256)
    img_acc = []

    for i, fpath in enumerate(file_paths):
        try:
            print(i)
            print(fpath)
            img = cv2.resize(cv2.imread(fpath, cv2.IMREAD_GRAYSCALE), img_size)
            img_acc.append(img)
        except Exception:
            pass

    return img_acc


def create_similar_groups(img_acc, list_files):
    if not isinstance(list_files, list):
        sel_files = list(list_files)  # [os.path.join(fold_path, x) for x in list_files]
    else:
        sel_files = list_files[:]
    relation = 0
    for i, img in enumerate(img_acc):
        searh_img = img_acc[:]
        init_path = [sel_files[i]]
        del sel_files[i]
        del searh_img[i]
        acc = [img]
        ind_del = []

        for j, s_img in enumerate(searh_img):
            result = compare_ssim(img, s_img)
            if result > 0.55:
                ind_del.append(j)
                init_path.append(sel_files[j])
                acc.append(s_img)
        if len(ind_del) > 0:
            # write files
            if len(ind_del) > 1:
                write_files_into_folder(init_path, acc, fold_path, relation)
            relation += 1
            # delete in search spaces
            for x in sorted(ind_del, reverse=True):
                del searh_img[x]
                del sel_files[x]
        img_acc = searh_img[:]


def compare_ssim_mult(chunk, L, thread_number, img_acc, root):
    _start_time = time.time()
    for pair in chunk:
        L.append({pair: compare_ssim(img_acc[pair], root)})
    t_sec = time.time() - _start_time
    print('Time passed: :{}sec'.format(t_sec))


#todo divide conquer is applied.
def main():
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--first", required=True, help="first input image")
    # ap.add_argument("-s", "--second", required=True, help="second")
    args = vars(ap.parse_args())
    fname = (args["first"].split("\\")[-1])
    #sname = (args["second"].split("\\")[-1]).split(".")[0]
    print(fname)
    img_dir = args["first"]
    dir_path = os.path.dirname(img_dir)
    fold_path = os.path.join(dir_path, fname+"_SSIM_analysis")
    Path(fold_path).mkdir(parents=True, exist_ok=True)
    # get images
    list_files = os.listdir(img_dir) #[:200]
    list_files.sort(reverse=True)
    fstart_paths = [os.path.join(img_dir, x) for x in list_files if os.path.isfile(os.path.join(img_dir, x))]
    selected_files = fstart_paths[:]

    n_sample = len(list_files)
    '''accumulate images'''
    print(fstart_paths)
    print('&')

    img_acc = accumulate_images(fstart_paths)

    print('LOOP')
    img_acc_org = img_acc[:]
    '''create groups of  similar matches'''
    with Manager() as manager:
        for i, img in enumerate(img_acc):

            print('*'*20)
            print("Root file:  {}".format(selected_files[i]))
            ind_to_del = []
            n_sample = len(img_acc)
            images_indexes = [x for x in range(i+1, n_sample)]
            random.shuffle(images_indexes) # why you need shuffling
            chunks = list(split(images_indexes, threads_count))

            threads = []
            # thred-safe list to store results
            lqueues = manager.list()
            for thread_number in range(threads_count):
                chunk = chunks[thread_number]
                t = Thread(target=compare_ssim_mult, args=(chunk, lqueues, thread_number, img_acc, img_acc[i]))
                t.start()
                threads.append(t)
            # waiting all the threads finished
            for t in threads:
                t.join()
            '''combination results index is not the same with index of img_acc'''
            combinations_result = {list(x.keys())[0]: list(x.values())[0] for x in lqueues}
            for j in range(i+1, n_sample):
                result = combinations_result[j]
                if result > 0.4:
                    ind_to_del.append(j)
            if ind_to_del:
                dst_fold_name = (selected_files[i].split("\\")[-1]).rsplit(".",1)[0]
                data_paths = [selected_files[x] for x in ind_to_del]
                print("=> Files {} moved to {}".format(str(data_paths), dst_fold_name))

                move_files(data_paths, fold_path, dst_fold_name)
                for x in sorted(ind_to_del, reverse=True):
                    print("- File {} excluded from the dataset".format(selected_files[x]))
                    del img_acc[x]
                    del selected_files[x]
            print('_')



if __name__ == '__main__':
    tic()
    main()
    tac()
