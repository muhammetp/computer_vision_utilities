import pandas as pd
from pathlib import Path
import cv2
import pickle
import numpy as np
from yaml_annotation_parser import create_annotations_pickle
import shutil
target_class = ['person_fallen', 'person_sitting', 'person_standing', 'snow', 'spill', 'vehicle','weapon', "handgun"]


def swap_min_max(bbox):
    bbox = [int(float(x)) for x in bbox]
    x1, y1, x2, y2 = bbox
    x1, x2 = min(x1, x2), max(x1, x2)
    y1, y2 = min(y1, y2), max(y1, y2)
    return x1, y1, x2, y2


def create_crop_img_folder_and_file_names():
    global target_class, img_dir_path, main_dir
    crop_img_dir = main_dir / f"{img_dir_path.name}_cropped"
    crop_img_pickle_name = main_dir / f"{crop_img_dir.name}.pickle"
    crop_img_dir.mkdir(parents=True, exist_ok=True)
    [(crop_img_dir / x).mkdir(parents=True, exist_ok=True) for x in target_class]
    return crop_img_dir, crop_img_pickle_name


def get_image_file_name_path_extension(fpath: Path):
    fpath = fpath.parts[-3:]
    fname, extension = fpath[-1].rsplit(".", 1)
    fpath = (img_dir_path / fpath[-3] / fpath[-2] / fpath[-1]).__str__()
    return fpath, fname, extension

def create_cropped_images(annotations):
    for annot in annotations:
        fpath, *bbox, label = annot
        x1, y1, x2, y2 = swap_min_max(bbox)
        fpath, fname, extension = get_image_file_name_path_extension(fpath)
        img = cv2.imread(fpath)
        crop_img = img[y1:y2,x1:x2]
        if not np.size(crop_img):
            print(f"{fpath} is not working!!!")
            continue
        class_dir_name = (crop_img_dir/label).__str__()
        if fpath == fpath_prev:
            obj_counter += 1
        else:
            obj_counter = 0
            fpath_prev = fpath
        fname = f"{class_dir_name}/{fname}_{label}_{obj_counter}.{extension}"
        cv2.imwrite(fname, crop_img)
        data_dict[label].append(fname)

    with open(crop_img_pickle_name, 'wb') as handle:
        pickle.dump(data_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
if __name__ == "__main__":
    img_dir_path = Path("C:/projects/data/full_size_master")
    main_dir = img_dir_path.parent.absolute()
    crop_img_dir, crop_img_pickle_name = create_crop_img_folder_and_file_names()
    annot_file_path = Path("C:/projects/data/full_size_master_annotations.pickle")
    if not annot_file_path.is_file():
        create_annotations_pickle(main_dir)

    annotations = pd.read_pickle(annot_file_path)

    data_dict = {label:[] for label in target_class}
    fpath_prev = None; obj_counter=0
    for annot in annotations:
        fpath, *bbox, label = annot
        class_dir_name = (crop_img_dir / label).__str__()
        #x1, y1, x2, y2 = swap_min_max(bbox)
        fpath, fname, extension = get_image_file_name_path_extension(fpath)
        fdst = f"{class_dir_name}/{fname}.{extension}"
        shutil.copyfile(fpath, fdst)
        data_dict[label].append(fdst)

    with open(crop_img_pickle_name, 'wb') as handle:
        pickle.dump(data_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)